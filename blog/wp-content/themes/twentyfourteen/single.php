<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
<div class="cs_mode_contain">
<div class="mode_container">
<div class="container_24">
<div class="two_column grid_24 omega alpha" id="columns">
<div id="left_column" class="grid_4  alpha fadeInLeft animated" data-animate="fadeInLeft" data-delay="0">
<?php
get_sidebar();
?>
</div>
<div id="center_column" class="grid_20 omega fadeInRight animated" data-delay="0" data-animate="fadeInRight">
<br>
<br>
<?php
	// Start the Loop.
	while ( have_posts() ) : the_post();

		/*
		 * Include the post format-specific template for the content. If you want to
		 * use this in a child theme, then include a file called called content-___.php
		 * (where ___ is the post format) and that will be used instead.
		 */
		?>
		<h3>
		<?php
		the_title();
		?>
		</h3>
	<?php the_post_thumbnail('', array('class' => 'align')); ?>
	<?php the_content(); ?>

	<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}
	endwhile;
?>
</div>
</div>
</div>
</div>
</div>
<?php
get_footer();
