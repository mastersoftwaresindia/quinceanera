<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		
		<div id="page">
	<div class="mode_header">
        <div class="mode_header_content">
        	<div class="container_24">
            	<div id="header" class="grid_24 clearfix omega alpha">
                	<div id="header_left" class="grid_8 alpha">
                        <ul id="header_links">
                        <li class="first"><a title="home" href="javascript:void(0);"><surfmarktag>Home</surfmarktag></a></li>
                        <li><a title="about us" href="javascript:void(0);"><surfmarktag>About us</surfmarktag></a></li>
                        <li id="header_link_sitemap"><a title="sitemap" href="javascript:void(0);"><surfmarktag>sitemap</surfmarktag></a></li>
                        <li id="header_link_contact" class="last"><a title="contact" href="javascript:void(0);"><surfmarktag>contact</surfmarktag></a></li>
                        </ul>
                    
                        <div class="free_shipping"><surfmarktag>Free shipping on orders of $150 + and free returns</surfmarktag>
                        </div>
                    </div>
                	<div id="header_content" class="grid_8"> <a title="Eggthemes WeddingDay Prestashop Theme" id="header_logo" href="javascript:void(0);"> <img alt="Eggthemes WeddingDay Prestashop Theme" src="<?php echo get_template_directory_uri(); ?>/img/logo.jpg" class="logo"> </a>
                        <ul class="cs_top_links">
                            <li><a title="" href="javascript:void(0);"><surfmarktag>Shop</surfmarktag></a></li>
                            <li><a title="" href="javascript:void(0);"><surfmarktag>LookBook</surfmarktag></a></li>
                            <li><a title="" href="javascript:void(0);"><surfmarktag>Blog</surfmarktag></a></li>
                            <li><a title="" href="javascript:void(0);"><surfmarktag>About</surfmarktag></a></li>
                            <li><a title="" href="javascript:void(0);"><surfmarktag>Features</surfmarktag></a></li>
                        </ul>
                    </div>
                	<div id="header_right" class="grid_8 omega">
                    
                        <div id="header_user">
                            <ul id="header_nav">
                                <li id="your_account"><a title="View my customer account" rel="nofollow" href="javascript:void(0);"><surfmarktag>My Account</surfmarktag></a></li>
                                <li id="header_user_info"><surfmarktag> Welcome
                                guest! </surfmarktag><a title="Login to your customer account" rel="nofollow" href="javascript:void(0);" class="login"><surfmarktag>Login</surfmarktag></a></li>
                                <li id="shopping_cart"> <a title="View my shopping cart" rel="nofollow" href="javascript:void(0);" style="border-radius: 3px;"><surfmarktag>Cart </surfmarktag>
                                
                                <span class="ajax_cart_quantity hidden" style="display: none;"><surfmarktag>0</surfmarktag>
                                </span> 
                                
                                <span class="ajax_cart_product_txt hidden" style="display: none;"><surfmarktag>Product</surfmarktag>
                                </span> 
                                
                                <span class="ajax_cart_product_txt_s hidden" style="display: none;"><surfmarktag>Products</surfmarktag>
                                </span> 
                                
                                <span class="ajax_cart_total hidden" style="display: none;">$0.00
                                </span> 
                                
                                <span class="ajax_cart_no_product"><surfmarktag>(empty)</surfmarktag>
                                </span> </a></li>
                            </ul>
                        </div>
                    
                    <div id="search_block_top">
                    
                    <form method="get" id="searchbox" action="">
                    
                    <p> 
                    <label for="search_query_top">
                    </label> 
                    <input type="hidden" value="search" name="controller"> 
                    <input type="hidden" value="position" name="orderby"> 
                    <input type="hidden" value="desc" name="orderway"> 
                    <input type="text" placeholder="Search" value="" name="search_query" id="search_query_top" class="search_query ac_input" autocomplete="off"> 
                    <input type="submit" class="button" value="Search" name="submit_search"></p>
                    </form>
                    </div>
                    <script type="text/javascript">/* &lt;![CDATA[ */$('document').ready(function(){$("#search_query_top").autocomplete('http://demo3.eggthemes.com/et_weddingday/style1/index.php?controller=search',{minChars:3,max:10,width:500,selectFirst:false,scroll:false,dataType:"json",formatItem:function(data,i,max,value,term){return value;},parse:function(data){var mytab=new Array();for(var i=0;i&lt;data.length;i++)
                    mytab[mytab.length]={data:data[i],value:data[i].cname+' &gt; '+data[i].pname};return mytab;},extraParams:{ajaxSearch:1,id_lang:1}}).result(function(event,data,formatted){$('#search_query_top').val(data.pname);document.location.href=data.product_link;})});/* ]]&gt; */
                    </script>
                    <script type="text/javascript">/* &lt;![CDATA[ */var CUSTOMIZE_TEXTFIELD=1;var img_dir='http://demo3.eggthemes.com/et_weddingday/style1/themes/et_weddingday/img/';/* ]]&gt; */
                    </script>
                    <script type="text/javascript">/* &lt;![CDATA[ */var customizationIdMessage='Customization #';var removingLinkText='Please remove this product from my cart.';var freeShippingTranslation='Free shipping!';var freeProductTranslation='Free!';var delete_txt='Delete';var generated_date=1406103313;/* ]]&gt; */
                    </script>
                    
                    <div id="cart_block" class="block exclusive"><h4 class="title_block"> 
                    <a title="View my shopping cart" rel="nofollow" href="javascript:void(0);"><surfmarktag>Cart</surfmarktag></a> 
                    
                    <span id="block_cart_expand" class="hidden">&nbsp;
                    </span> 
                    
                    <span id="block_cart_collapse">&nbsp;
                    </span></h4>
                    
                    <div class="block_content">
                    
                    <div id="cart_block_summary" class="collapsed"> 
                    
                    <span style="display: none;" class="ajax_cart_quantity"><surfmarktag>0</surfmarktag>
                    </span> 
                    
                    <span style="display: none;" class="ajax_cart_product_txt_s"><surfmarktag>Products</surfmarktag>
                    </span> 
                    
                    <span class="ajax_cart_product_txt" style="display: none;"><surfmarktag>Product</surfmarktag>
                    </span> 
                    
                    <span style="display: none;" class="ajax_cart_total">$0.00
                    </span> 
                    
                    <span class="ajax_cart_no_product" style="display: inline-block;"><surfmarktag>(empty)</surfmarktag>
                    </span>
                    </div>
                    
                    <div id="cart_block_list" class="expanded">
                    
                    <p id="cart_block_no_products"><surfmarktag>No products</surfmarktag></p>
                    
                    <div id="cart-prices">
                    
                    <p>
                    
                    <span id="cart_block_shipping_cost" class="price ajax_cart_shipping_cost">$0.00
                    </span> 
                    
                    <span><surfmarktag>Shipping</surfmarktag>
                    </span></p>
                    
                    <p> 
                    
                    <span id="cart_block_total" class="price ajax_block_cart_total">$0.00
                    </span> 
                    
                    <span><surfmarktag>Total</surfmarktag>
                    </span></p>
                    </div>
                    
                    <p id="cart-buttons"> <a title="Checkout" rel="nofollow" id="button_order_cart" href="javascript:void(0);" class="exclusive_large"><surfmarktag>Checkout</surfmarktag></a></p>
                    </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="cs_both_mode">
	<div class="mode_megamenu">
    	<div class="container_24">
        
            <div id="menu" class="cs_mega_menu" style="display: block;">
                <ul class="ul_mega_menu">
                <li class=" menu_item menu_first level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent">
		<surfmarktag> New Arrivals</surfmarktag></a>
                
                <div style="width: 798.644px; margin-left: 0px; display: none;" class="options_list">
                
                <div style="width: 798.644px; float: left;" class="option">
                
                <div class="div_static">
                
                <div class="static_menu_6"><h3><surfmarktag>Online WeddingDay for Men and Women</surfmarktag></h3>
                
                <div class="static_content"><surfmarktag>Any shopaholic would fall in love with this layout, ET WeddingDay Responsive Prestashop theme. Super neat design, really modern and high fashion skin for the website. With variety available colors, probably, your website will be congruent with your store image. Need to change? You want a very own color combination? Pieces of cake, you can change it to any color you wish with color variation function. .</surfmarktag>
                </div><h3 class="title_2"><surfmarktag>ET WeddingDay description</surfmarktag></h3>
                
                <div class="static_content"><surfmarktag>Are you wondering how about its functionality? You will be truly impressed with its features. The most noteworthy thing is responsive layout which will automatically resize layout when changing screen size. Large size slideshow plus promotion blocks blow fresh air into the website and to make it more glamour and magnificent. Vertical navigation sorts all categories in the most logical way which is shown along the way visitors scroll down. Extremely flexible product widget and latest view widget will save you massive time in managing and administrating website and products. Advance layered navigation extension is a stunning filter for visit to search for closest results.</surfmarktag>
                </div>
                </div>
                </div>
                </div> 
                
                <span style="width: 800px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 800px;" class="spanOptionList">
                </span></li>
                <li class=" menu_item level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Men's</surfmarktag></a>
                
                <div style="width: 349.407px; margin-left: 0px; display: none;" class="options_list">
                
                <div style="width: 349.407px; float: left;" class="option">
                <ul style="width: 349.407px;" class="column product ">
                <li class="ajax_block_product">
                
                <div class="center_block">
                
                <div class="image"> <a title="Quisque pharetra faucibus ante ut pretium" href="javascript:void(0);" class="product_image_menu"> <img alt="Quisque pharetra faucibus ante ut pretium" src="163-medium_default.jpg"> </a>
                </div>
                
                <div class="name_product"><h3><a title="Quisque pharetra faucibus ante ut pretium" href="javascript:void(0);" alt="Quisque pharetra faucibus ante ut pretium"><surfmarktag>Quisque pharetra faucibus ante ut...</surfmarktag></a></h3>
                </div>
                
                <p class="price_container">
                
                <span class="price"><surfmarktag>$98.00</surfmarktag>
                </span></p>
                </div></li>
                <li class="ajax_block_product">
                
                <div class="center_block">
                
                <div class="image"> <a title="Integer condimentum ante quis  augue vestibulum" href="javascript:void(0);" class="product_image_menu"> <img alt="Integer condimentum ante quis  augue vestibulum" src="170-medium_default.jpg"> </a>
                </div>
                
                <div class="name_product"><h3><a title="Integer condimentum ante quis  augue vestibulum" href="javascript:void(0);" alt="Integer condimentum ante quis  augue vestibulum"><surfmarktag>Integer condimentum ante quis  augue...</surfmarktag></a></h3>
                </div>
                
                <p class="price_container">
                
                <span class="price"><surfmarktag>$98.00</surfmarktag>
                </span></p>
                </div></li>
                </ul> 
                
                <span style="width: 350px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 350px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 350px;" class="spanOptionList">
                </span></li>
                <li class=" menu_item level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Women's</surfmarktag></a>
                
                <div style="width: 199.661px; margin-left: 0px; display: none;" class="options_list">
                
                <div style="width: 199.661px; float: left;" class="option">
                <ul style="width: 199.661px;" class="column level_0">
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Eliquam elementum tellus</surfmarktag></a></li>
                <li class="category_item parent"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam elementum</surfmarktag></a>
                
                <div style="width: 200px; margin-left: 0px;" class="sub_menu">
                <ul class="level_1" style="width: 199.661px;">
                <li><a href="javascript:void(0);"><surfmarktag> turpis porta at ultricie</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> Praesent sollicitudin</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> sem neque vestibu</surfmarktag></a></li>
                </ul>
                </div></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Porta at ultricies diam</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam nibh ligula</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Nam in magna a lorem</surfmarktag></a></li>
                </ul> 
                
                <span style="width: 200px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 200px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 200px;" class="spanOptionList">
                </span></li>
                <li class=" menu_item level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Dresses</surfmarktag></a>
                
                <div style="width: 1178px; margin-left: -447.5px; display: none;" class="options_list">
                
                <div style="width: 392.666px; float: left;" class="option">
                
                <div class="div_static">
                
                <div class="static_menu_6">
                
                <p class="cs_st_img"><img width="216" height="84" alt="" src="m_chis.png"></p>
                
                <p><surfmarktag>Any shopaholic would fall in love with this layout, ET Boutique Responsive Prestashop theme. Super neat design, really modern and high fashion skin for the website. With variety available colors, probably, your website will be congruent with your store image. Need to change? You want a very own color combination? Pieces of cake, you can change it to any color you wish with color variation function.</surfmarktag></p>
                </div>
                </div>
                </div> 
                
                <span style="width: 393.333px;" class="spanOption">
                </span>
                
                <div style="width: 196.334px; float: left;" class="option">
                <ul style="width: 196.334px;" class="column product ">
                <li class="ajax_block_product">
                
                <div class="center_block">
                
                <div class="image"> <a title="Integer condimentum ante quis  augue vestibulum" href="javascript:void(0);" class="product_image_menu"> <img alt="Integer condimentum ante quis  augue vestibulum" src="170-medium_default.jpg"> </a>
                </div>
                
                <div class="name_product"><h3><a title="Integer condimentum ante quis  augue vestibulum" href="javascript:void(0);" alt="Integer condimentum ante quis  augue vestibulum"><surfmarktag>Integer condimentum ante quis  augue...</surfmarktag></a></h3>
                </div>
                
                <p class="price_container">
                
                <span class="price"><surfmarktag>$98.00</surfmarktag>
                </span></p>
                </div></li>
                </ul> 
                
                <span style="width: 196.667px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 196.667px;" class="spanOption">
                </span>
                
                <div style="width: 196.334px; float: left;" class="option">
                <ul style="width: 196.334px;" class="column level_0">
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Eliquam elementum tellus</surfmarktag></a></li>
                <li class="category_item parent"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam elementum</surfmarktag></a>
                
                <div style="width: 196.667px; margin-left: 0px;" class="sub_menu">
                <ul class="level_1" style="width: 196.334px;">
                <li><a href="javascript:void(0);"><surfmarktag> turpis porta at ultricie</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> Praesent sollicitudin</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> sem neque vestibu</surfmarktag></a></li>
                </ul>
                </div></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Porta at ultricies diam</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam nibh ligula</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Nam in magna a lorem</surfmarktag></a></li>
                </ul> 
                
                <span style="width: 196.667px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 196.667px;" class="spanOption">
                </span>
                
                <div style="width: 196.334px; float: left;" class="option">
                <ul style="width: 196.334px;" class="column level_0">
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Eliquam elementum tellus</surfmarktag></a></li>
                <li class="category_item parent"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam elementum</surfmarktag></a>
                
                <div style="width: 196.667px; margin-left: 0px;" class="sub_menu">
                <ul class="level_1" style="width: 196.334px;">
                <li><a href="javascript:void(0);"><surfmarktag> turpis porta at ultricie</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> Praesent sollicitudin</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> sem neque vestibu</surfmarktag></a></li>
                </ul>
                </div></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Porta at ultricies diam</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam nibh ligula</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Nam in magna a lorem</surfmarktag></a></li>
                </ul> 
                
                <span style="width: 196.667px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 196.667px;" class="spanOption">
                </span>
                
                <div style="width: 196.334px; float: left;" class="option">
                <ul style="width: 196.334px;" class="column level_0">
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> turpis porta at ultricie</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Praesent sollicitudin</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> sem neque vestibu</surfmarktag></a></li>
                </ul> 
                
                <span style="width: 196.667px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 196.667px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 1180px;" class="spanOptionList">
                </span></li>
                <li class="cs_m_manufacter menu_item level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Outerwear</surfmarktag></a>
                
                <div style="width: 698.814px; margin-left: -97.5px; display: none;" class="options_list">
                
                <div style="width: 698.814px; float: left;" class="option">
                <ul style="width: 698.814px;" class="column manufacture">
                <li class="product_item"> <a title="Azzaro" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/3-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="Boss" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/4-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="Bugati" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/5-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="Calvin Klein" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/6-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="Chris" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/7-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="Clinique" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/8-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="dkny" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/9-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="estee" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/10-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="paul smith" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/11-manu_default.jpg"></a></li>
                <li class="product_item"> <a title="tomy" href="javascript:void(0);" class="img_manu"> <img alt="" src="img/12-manu_default.jpg"></a></li>
                </ul> 
                
                <span style="width: 700px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 700px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 700px;" class="spanOptionList">
                </span></li>
                <li class="cs_hide_p menu_item level-1 parent"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Best Seller</surfmarktag></a>
                
                <div style="width: 249.576px; margin-left: 0px; display: none;" class="options_list">
                
                <div style="width: 249.576px; float: left;" class="option">
                <ul style="width: 249.576px;" class="column product ">
                <li class="ajax_block_product">
                
                <div class="center_block">
                
                <div class="image"> <a title="Fusce placerat erat sit amet tellus facilisis" href="javascript:void(0);" class="product_image_menu"> <img alt="Fusce placerat erat sit amet tellus facilisis" src="197-home_default.jpg"> </a>
                </div>
                
                <div class="name_product"><h3><a title="Fusce placerat erat sit amet tellus facilisis" href="javascript:void(0);" alt="Fusce placerat erat sit amet tellus facilisis"><surfmarktag>Fusce placerat erat sit amet tellus...</surfmarktag></a></h3>
                </div>
                
                <p class="price_container">
                
                <span class="price"><surfmarktag>$97.00</surfmarktag>
                </span></p>
                </div></li>
                </ul> 
                
                <span style="width: 250px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 250px;" class="spanOption">
                </span>
                
                <div style="width: 249.576px; float: left;" class="option">
                <ul style="width: 249.576px;" class="column level_0">
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Eliquam elementum tellus</surfmarktag></a></li>
                <li class="category_item parent"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam elementum</surfmarktag></a>
                
                <div style="width: 250px; margin-left: 0px;" class="sub_menu">
                <ul class="level_1" style="width: 249.576px;">
                <li><a href="javascript:void(0);"><surfmarktag> turpis porta at ultricie</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> Praesent sollicitudin</surfmarktag></a></li>
                <li><a href="javascript:void(0);"><surfmarktag> sem neque vestibu</surfmarktag></a></li>
                </ul>
                </div></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Porta at ultricies diam</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Aliquam nibh ligula</surfmarktag></a></li>
                <li class="category_item"> <a href="javascript:void(0);" class="cat_child"><surfmarktag> Nam in magna a lorem</surfmarktag></a></li>
                </ul> 
                
                <span style="width: 250px;" class="spanColumn">
                </span>
                </div> 
                
                <span style="width: 250px;" class="spanOption">
                </span>
                </div> 
                
                <span style="width: 250px;" class="spanOptionList">
                </span></li>
                <li class=" menu_item level-1"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Sale</surfmarktag></a></li>
                <li class=" menu_item level-1"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Shoes</surfmarktag></a></li>
                <li class=" menu_item menu_last level-1"> <a href="javascript:void(0);" class="title_menu_parent"><surfmarktag> Blog</surfmarktag></a></li>
                </ul>
            </div>
        <script type="text/javascript">/* &lt;![CDATA[ */var numLiItem=0;numLiItem=$("#menu ul li.level-1").length;var moreInsert1=6;var moreInsert2=5;var htmlLiHide1=getHtmlHide1(moreInsert1,numLiItem);var htmlLiHide2=getHtmlHide2(moreInsert2,numLiItem);var htmlMenu=$("#menu ul.ul_mega_menu").html();if(moreInsert1&gt;0&amp;&amp;moreInsert2&gt;0)
        {window.onorientationchange=function(){refeshMenuIpad(moreInsert1,moreInsert2,htmlLiHide1,htmlLiHide2);addMoreOnLoad(moreInsert1,moreInsert2,numLiItem,htmlLiHide1,htmlLiHide2);}}/* ]]&gt; */
        </script>
        
            <div id="megamenu-responsive" style="display: none;">
                <ul id="megamenu-responsive-root">
                    <li class="menu-toggle">
                    
                    <p></p><surfmarktag>Navigation</surfmarktag></li>
                    <li class="root">
                <ul>
                <li class=""><a href="javascript:void(0);">
            
                <span><surfmarktag>Home</surfmarktag>
                </span></a>
            <ul>
            <li class=""><a href="javascript:void(0);"><span><surfmarktag>Men's</surfmarktag>
            </span></a></li>
            <li class="parent "><p>+</p>
            
            <p>+</p>
            
            <p>+</p><a href="javascript:void(0);">
            
            <span><surfmarktag>Women's</surfmarktag>
            </span></a>
            <ul>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Eliquam elementum tellus</surfmarktag>
            </span></a></li>
            <li class="parent "><p>+</p>
            
            <p>+</p>
            
            <p>+</p><a href="javascript:void(0);">
            
            <span><surfmarktag>Aliquam elementum</surfmarktag>
            </span></a>
            <ul>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag> turpis porta at ultricie</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag> Praesent sollicitudin</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>sem neque vestibu</surfmarktag>
            </span></a></li>
            </ul></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Porta at ultricies diam</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Aliquam nibh ligula</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Nam in magna a lorem</surfmarktag>
            </span></a></li>
            </ul></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Dresses</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>outerwear</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Shoes</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Jewelry</surfmarktag>
            </span></a></li>
            <li class=""><a href="javascript:void(0);">
            
            <span><surfmarktag>Accessories</surfmarktag>
            </span></a></li>
            </ul></li>
            </ul></li>
            </ul>
            </div>
        </div>
    </div>


</div></div></div></div></div></div>
		<!--
		<header id="masthead" class="site-header" role="banner">
			<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			</a>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><!-- #masthead -->!->

		<div id="main" class="site-main">
