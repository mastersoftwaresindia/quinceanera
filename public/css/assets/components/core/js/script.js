/**
 *
 * HTML5 Image uploader with Jcrop
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2012, Script Tutorials
 * http://www.script-tutorials.com/
 */

// convert bytes into friendly format
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
};

// check for selected crop region
function checkForm() {
    if($('#image_file').val()) return true;
    $('.error').html('Please select a file to upload !!!').show();
    return false;

    if (parseInt($('#w').val())) return true;
    $('.error').html('Please select a crop region and then press Upload').show();
    return false;
};

// update info by cropping (onChange and onSelect events handler)
function updateInfo(e) {
    $('#x1').val(e.x);
    $('#y1').val(e.y);
    $('#x2').val(e.x2);
    $('#y2').val(e.y2);
    $('#w').val(e.w);
    $('#h').val(e.h);
    if (parseInt(e.w) > 0) {
        var rx = xsize / e.w;
        var ry = ysize / e.h;
        $pimg.css({
          width: Math.round(rx * boundx) + 'px',
          height: Math.round(ry * boundy) + 'px',
          marginLeft: '-' + Math.round(rx * e.x) + 'px',
          marginTop: '-' + Math.round(ry * e.y) + 'px'
        });
    }
};

// clear info by cropping (onRelease event handler)
function clearInfo() {
    $('.info #w').val('');
    $('.info #h').val('');
};

// Create variables (in this scope) to hold the API and image size
var jcrop_api,
    boundx,
    boundy,
    // Grab some information about the preview pane
    $preview = $('#preview-pane'),
    $pcnt = $('#preview-pane .preview-container'),
    $pimg = $('#preview-pane .preview-container img'),

    xsize = $pcnt.width(),
    ysize = $pcnt.height();

function fileSelectHandler() {
    var process = false;
    function getRandom() {
      var dim = jcrop_api.getBounds();
      return [
        Math.round(Math.random() * dim[0]),
        Math.round(Math.random() * dim[1]),
        Math.round(Math.random() * dim[0]),
        Math.round(Math.random() * dim[1])
      ];
    };
    // get selected file
    var oFile = $('#image_file')[0].files[0];

    // hide all errors
    $('.error').hide();

    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png|image\/jpg|image\/gif)$/i;
    if (! rFilter.test(oFile.type)) {
        $('.error').html('Please select a valid image file (jpg, png, jpg and gif are allowed)').show();
        return;
    }

    // check for file size
    //if (oFile.size > 250 * 1024) {
        //$('.error').html('You have selected too big file, please select a one smaller image file').show();
        //return;
    //}

    // preview element
    var oImage = document.getElementById('preview');
    
    $(".error").html("Uploading").promise().done(function(){
            //your callback logic / code here
            //$('.error').html('You have selected too big file, please select a one smaller image file').show();
            // prepare HTML5 FileReader
            if (window.FileReader) {
                
            var oReader = new FileReader();
            oReader.onload = function(e) {

            // e.target.result contains the DataURL which we can use as a source of the image
            oImage.src = e.target.result;
            $(".jcrop-preview").attr('src',e.target.result);
            // oImage.onload = function () { // onload event handler
                // display step 2
                $(".step2").removeClass('hide');
                $('.step2').fadeIn(500);

                // display some basic image info
                var sResultFileSize = bytesToSize(oFile.size);
                $('#filesize').val(sResultFileSize);
                $('#filetype').val(oFile.type);
                $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

                // destroy Jcrop if it is existed
                if (typeof jcrop_api != 'undefined') {
                    jcrop_api.destroy();
                    jcrop_api = null;
                    $('#preview').css({
                        'display'    : '',
                        'height'     : '',
                        'visibility' : '',
                        'width'      : ''
                    });
                    $(".jcrop-holder").remove();
                    $('#preview').width(oImage.naturalWidth);
                    $('#preview').height(oImage.naturalHeight);
                }

                setTimeout(function(){
                    // initialize Jcrop
                    console.log('init',[xsize,ysize]);
                    $('#preview').Jcrop({
                        minSize: [32, 32], // min crop size
                        boxWidth: 700,
                        boxHeight: 500,
                        aspectRatio : xsize / ysize, // keep aspect ratio 1:1
                        bgFade: true, // use fade effect
                        bgOpacity: .3, // fade opacity
                        bgColor: 'transparent',
                        onChange: updateInfo,
                        onSelect: updateInfo,
                        onRelease: clearInfo
                    }, function(){
                        // use the Jcrop API to get the real image size
                        var bounds = this.getBounds();
                        console.log(bounds, "bounds");
                        boundx = bounds[0];
                        boundy = bounds[1];

                        // Store the Jcrop API in the jcrop_api variable
                        jcrop_api = this;
                        jcrop_api.animateTo(getRandom());
                        // Move the preview into the jcrop container for css positioning
                        $preview.appendTo(jcrop_api.ui.holder);
                        $("#preview-pane").show();
                        $(".step2").addClass('hide');
			$("#form-submit1").show();
                        $("#main-uploader").removeAttr("disabled");
                        
                /* 
                    This is standard Media Handler.  @Jeevan
                */
                  $("#form-submit").click(function(e){
                      e.preventDefault();   
                      $("#upload_form").ajaxForm({
                            target: "#preview",
                            success: function(data){
                                $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                                    $(this).animate({ scrollTop: 0 }, 1000);                                                    
                                    $("#final_image").html(data);
                        			    process =false;
                        				//setTimeout(function(){  
                        				//	location.reload();
                        				//}, 2000);					    
                                });
                            }    
                      }).submit();
                      
                  });

              
                          /* 
                            This is standard Media Handler.  @Dinesh
                          */
                          if ( !process ) {
                            $("#form-submit1").click(function(e){
                              e.preventDefault();
                              $("#upload_form1").ajaxForm({
                                  target: "#preview",
                                  success: function(data){
                                        $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                                            $(this).animate({ scrollTop: 0 }, 1000);
					    $("#final_image").html(data);
                                            var type = $("a#split_image").attr('type');
                                            var user = $("a#split_image").attr('user');
                                            $(".step2").addClass('hide');
                                            jcrop_api.destroy();
                                            $(".jcrop-holder").remove();
					    $.ajax({
						    type: "GET",
						    url: "http://"+document.location.hostname+"/za/public/admin/media-uploader/index/format/html",
						    data: { user: user, section: 'user-avatar' }
						})
						.done(function( msg ) {
						    $('div.image_data').html(msg);
						    $('#preview').css({
								  'display'    : 'none',
								  'height'     : '',
								  'visibility' : '',
								  'width'      : ''
						    });
						    $("#uplimage , .input-append").hide();
						    process =false;
						});
                                            
                                        });
                                    }    
                              }).submit();
                              
                          });
                        }
                    });
                },3000);          
    
           // };
        };
        // read selected file as DataURL
        oReader.readAsDataURL(oFile);
            } else {
                alert("The browser does not support Filereader data api, Please update your browser !");
            }
    });
    
}

