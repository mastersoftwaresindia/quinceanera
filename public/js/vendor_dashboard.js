
jQuery(document).ready(function($){
     var url_path = '/';    
     $('#profile_form').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh',
	
          },    
          fields: {
               fname: {
                    validators: {
                         notEmpty: {
                              message: 'The first name is required and can\'t be empty'
                         }
                    }
               },
               lname: {
                    validators: {
                         notEmpty: {
                              message: 'The last name is required and can\'t be empty'
                         }
                    }
               },
               email: {
                    validators: {
                         notEmpty: {
                              message: 'The email is required and can\'t be empty'
                         }
               }
               },
               address: {
                    validators: {
                         notEmpty: {
                              message: 'The address is required and can\'t be empty'
                         }
                    }
               },
               city: {
                    validators: {
                         notEmpty: {
                              message: 'The city is required and can\'t be empty'
                         }
                    }
               },
               state: {
                    validators: {
                         notEmpty: {
                              message: 'The state is required and can\'t be empty'
                         }
                    }
               },
               company_name: {
                    validators: {
                         notEmpty: {
                              message: 'The company name is required and can\'t be empty'
                         }
                    }
               },           
               phone: {
                    validators: {
                         notEmpty: {
                              message: 'The phone number is required and can\'t be empty'
                         },
                         integer: {
                              message: " Phone number must contain digits only",                                
                         },
			 maxlength:10
               }},
               fax: {
                    validators: {
                         integer: {
                              message: " Fax number must contain digits only",                                
                         }
                    }
               },
               zipcode: {
                    validators: {
                         integer: {
                              message: " Zipcode must contain digits only",                                
                         }
                    }
               },
               website: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               },
		fb_url: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               },
               twitter_url: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               },
		gplus_url: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               },
	    pinterest_url: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               },  instagram_url: {
                        validators: {
                              uri: {
                                  message: 'Enter website URL beginning with http or https'
                              }
                         }
               }                 
          }      
        
     });
    
     $('#vendor_login').bootstrapValidator({
          message: 'This value is not valid',
          feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
          },
          fields: {            
               email: {
                    trigger: 'blur', 
                    validators: {
                        notEmpty: {
                                     message: 'The email address is required and can\'t be empty'
                                  },
                                    emailAddress: {
                                        message: 'The input is not a valid email address'
                                    }
                                }
               },
               password: {
                    validators: {
                         notEmpty: {
                              message: 'The password is required and can\'t be empty'
                         }               
                    }
               }            
          }
     });

     $("#account-videos").find('ul li a.delete_data').each(function(index){
		       
               $(this).click(function(){          
               var answer = confirm("Are you sure you want to delete this video ?");
               if (answer) {
                    var id = $(this).attr('id');
                    $.ajax({
                         type: "GET",
                         url: "/admin/media-uploader/del/format/html",
                         data: { id: id }
                    })
                    .done(function( msg ) {
                         $('#vendor_video_'+id).hide();
                         location.reload();
                    });                     
               }               
          })
          
     });

     $("#up_video").click(function(e){ 
          e.preventDefault();   
          $("#video_upload").ajaxForm({
               target: "#final_image1", 
               success: function(data){ 
                    $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                         $(this).animate({ scrollTop: 0 }, 1000);                                               
                         //$("#final_image1").html('<div class="alert alert-success" id="notification">'+ data +'&nbsp;&nbsp;</div>');
                         $("#final_image1").removeClass('hide');     
                         // location.reload();
                    });
               }    
          }).submit();
     });

     $("#add_album").click(function(e){ 
          e.preventDefault();   
          $("#album_upload").ajaxForm({
               target: "#final_image2", 
               success: function(data){ 
                    $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                    $(this).animate({ scrollTop: 0 }, 1000)
                                         
                    $("#final_image2").removeClass('hide');
                         location.reload();    
                    });                                      
               }    
          }).submit();                              
     });

     $("ul.album_actions").find('li a.delete_data').each(function(index){
	       
          $(this).click(function(){            
               var answer = confirm("Are you sure you want to delete this Album ?");
               if (answer) {
                 var id = $(this).attr('id');
                 //alert(id);
                 $.ajax({
                         type: "GET",
                         url: "/admin/media-uploader/delalbum/format/html",
                         data: { id: id }
                    })
                    .done(function( msg ) {
                        //alert("ok it is done");
                         $('#vendor_album_'+id).hide();
                         location.reload();    
                    });                     
               }               
          })
          
     });
     
     $(".album_det").find('a.gallery_image').each(function(index){       
          $(this).click(function(){               
               var answer = confirm("Are you sure you want to delete this Image ?");
               if (answer) {
                 var id = $(this).attr('id');
                 //alert(id);
                 $.ajax({
                         type: "GET",
                         url: "/admin/media-uploader/delimage/format/html",
                         data: { id: id }
                    })
                    .done(function( msg ) {
                       // alert("ok it is done");
                         $('#vendor_album_'+id).hide();
                         location.reload();                        
                    });                     
               }               
          })
          
     });

});


