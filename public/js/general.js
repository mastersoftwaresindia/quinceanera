jQuery(document).ready(function($){
     var url_path = '/';
     $("a#sign_up").click(function(e){
        
          $("#create_account").load(url_path+'users/index/signup/format/html', function(){
           
           $(this).lightbox_me({
            
                centered: true,
                onLoad: function() { 
                    $(this).find('.form input:first').focus()
                    $(this).removeClass('hide');
                    //valiadate();
                    $('.close').click(function(){
                        
                        $(this).trigger('close');    
                        
                    })
                    $('.form').bootstrapValidator({
                        submitHandler: function(validator, form, submitButton) {
                        // validator is the BootstrapValidator instance
                        // form is the jQuery object present the current form
                            var opts = {
                                success: function(data) {
                                    if($.trim(data) == 'Email id already exists.'){
                                       $('#preview').html(data);
                                       setTimeout(function(){
                                            $('#preview').html("");
                                       }, 3000);
                                    }else {
                                       $('#preview').html(data); 
                                    }
                                    
                                }
                            };
                            $(form).ajaxSubmit(opts);
                        },
                        message: 'This value is not valid',
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                            email: {
				trigger: 'blur',
                                validators: {
                                    notEmpty: {
                                        message: 'The email address is required and can\'t be empty'
                                    },
                                    emailAddress: {
                                        message: 'The input is not a valid email address'
                                    }
                                }
                            },
                            password: {
                                validators: {
                                    notEmpty: {
                                        message: 'The password is required and can\'t be empty'
                                    }
                                }
                            },
                            confirmPassword: {
                                validators: {
                                    notEmpty: {
                                        message: 'The confirm password is required and can\'t be empty'
                                    },
                                    identical: {
                                        field: 'password',
                                        message: 'The password and its confirm are not the same'
                                    }
                                }
                            }
                        }
                    });
                },
                closeClick: false,
                appearEase: "swing",
                
           }); 
        });
        e.preventDefault();
        
     });
    
  $("a#login").click(function(e){
        
        $("#create_account").load(url_path + 'users/index/signin/format/html', function(){   //load data from given location
        
           $(this).lightbox_me({
            
                centered: true,
                onLoad: function() { 
                    $(this).find('.form input:first').focus()  //focus on first input field
                    $(this).removeClass('hide');      
                    $('.close').click(function(){
                        
                        $(this).trigger('close');    //event ->close
                        
                    })


                    $('.form').bootstrapValidator({
                        submitHandler: function(validator, form, submitButton) {
                        // validator is the BootstrapValidator instance
                        // form is the jQuery object present the current form
                            var opts = {
                                success: function(data) {
                                console.log(data);
                                   $('#preview').html(data).promise().done(function(){
                                        //your callback logic / code here
                                        $('input[type="password"]').val('');
                                   });
                                }
                            };
                            $(form).ajaxSubmit(opts);
                        },

                        message: 'This value is not valid',
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },

                        fields: {
                            email: {
                                trigger: 'blur', 
                                validators: {
                                    notEmpty: {
                                        message: 'The email address is required and can\'t be empty'
                                    },
                                    emailAddress: {
                                        message: 'The input is not a valid email address'
                                    }
                                }
                            },
                            password: {
                                validators: {
                                    notEmpty: {
                                        message: 'The password is required and can\'t be empty'
                                    }
                                }
                            },
   
                        }
                    });
        },
                closeClick: false,
                appearEase: "swing",
                
           }); 
        });

        e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks
        
    });

  /* function to get user info after account activation. */
     $("a.continue").click(function(e){
        if(!$(".metype").val() && $(e.target).parent().parent().parent().hasClass('first-step')){
            alert("Please select an option.");
            return false;
        }else if(!$(".me-religion").val() && $(e.target).parent().parent().parent().hasClass('third-step')){
            alert("Please select an option.");
            return false;            
        }else if(!$("input[name='ceremony']").is(':checked') && $(e.target).parent().parent().parent().hasClass('fourth-step')){
            alert("Please select an option.");
            return false; 
        }else if(!$(".my-damas").val() && $(e.target).parent().parent().parent().hasClass('fifth-step')){
            return false;
        }else if(!$(".chambelanes").val() && $(e.target).parent().parent().parent().hasClass('sixth-step')){
            return false;
        }else if($(e.target).parent().parent().parent().hasClass('seventh-step')){
    
          if(!$("#myValue1").val() && !$("#myValue2").val()){
                alert("Please select atleast one value");
                return false; }
        }else if($(e.target).parent().parent().parent().hasClass('eigth-step')){
                   if(!$("#myValue3").val() && !$("#myValue4").val()){
                alert("Please select atleast one value");
                return false; }
        }else if($(e.target).parent().parent().parent().hasClass('ninth-step')){
            
            if($("#guests").val() < 0 ){
                alert("Please enter positive integer value.");
                return false;     
            }else if(!$("#guests").val()){
                alert("Please enter integer value.");
                return false; 
            }

        }else if(!$(".saving-plan").val() && $(e.target).parent().parent().parent().hasClass('twelve-step')){
            alert("Please select an option.");
            return false;
        }else if(!$("input[type='checkbox'][name='contactss[]']:checked").length > 0 && $(e.target).parent().parent().parent().hasClass('fourteen-step')){
            alert("Please select atleast one option.");
            return false;      
        }

        if($(e.target).parent().parent().parent().next().hasClass('setinfo')){
            $('.setinfo').removeClass('active');
            $('.setinfo').css('display', 'none');
            $(e.target).parent().parent().parent().next().addClass("active");
          
        }
    })

    $("a.skip").click(function(e){

	if($(".metype").val() && $(e.target).parent().parent().parent().hasClass('first-step')){
            $('.metype').val("").trigger('chosen:updated');
        }else if($(".me-religion").val() && $(e.target).parent().parent().parent().hasClass('third-step')){
            $('.me-religion').val("").trigger('chosen:updated');         
        }else if($("input[name='ceremony']").is(':checked') && $(e.target).parent().parent().parent().hasClass('fourth-step')){
            $("input[name='ceremony']").attr('checked', false);
        }else if($(e.target).parent().parent().parent().hasClass('seventh-step')){
            var len = $("#pickset :input").length;
            var inputColors = ['picker-1-0-color-cont', 'picker-1-1-color-cont'];
            for (var ix = 0; ix < len; ix++) {

               if($("input[name='"+inputColors[ix]+"']").val()){
                    $("input[name='"+inputColors[ix]+"']").val("");
               }
            }
        }if($(e.target).parent().parent().parent().hasClass('eigth-step')){
          
        }else if($(e.target).parent().parent().parent().hasClass('ninth-step')){
            
            if($("#guests").val()){
               $("#guests").val("");    
            }

        }else if($(e.target).parent().parent().parent().hasClass('tenth-step')){
            if($("#spentmoney").val()){
                $("#spentmoney").val("");     
            }
        }else if($(e.target).parent().parent().parent().hasClass('eleventh-step')){
            if($("#moneysofor").val()){
                $("#moneysofor").val("");     
            }
        }else if($(".saving-plan").val() && $(e.target).parent().parent().parent().hasClass('twelve-step')){
            $('.saving-plan').val("").trigger('chosen:updated');
        }else if($("input[type='checkbox'][name='contactss[]']:checked").length > 0 && $(e.target).parent().parent().parent().hasClass('fourteen-step')){
            $("input[type='checkbox'][name='contactss']").prop('checked', false);      
        }


        if($(e.target).parent().parent().parent().next().hasClass('setinfo')){
            $('.setinfo').removeClass('active');
            $('.setinfo').css('display', 'none');
            $(e.target).parent().parent().parent().next().addClass("active");
            
            if($(e.target).parent().parent().prev().prev().is("#div3_example")){
               $("#userdate").val(""); 
            }
         
        }
    })

    $(".back").click(function(e){
        if($(e.target).parent().parent().parent().prev().hasClass('setinfo')){
            $('.setinfo').removeClass('active');
            $('.setinfo').css('display', 'none');
            $(e.target).parent().parent().parent().prev().addClass("active");
            if($(e.target).parent().parent().parent().prev().find("#final_image")){
                // $("#upload_form1").attr("action", "http://"+document.location.hostname+"/za/public/admin/media-uploader/index/format/html");
            }
        }
    })

//    /* color picker for input box */
//
//	$("#wheel").colorWheel({
//		// controlsSelector: '.wheel-controls',
//		wheelWidth: 600,
//		pickedColorLables: ['Wedding theme 1', 'Wedding dress 2'],
//		initialColors: ['', ''],
//		addInputs: true,
//		addLabels: true,
//		enableKeyboardShortcuts: true,
//		pickContElement: 'div',
//		pickLabelElement: 'strong',
//		pickTextElement: 'span',
//		pickItemTitleText: 'Click to change this color'
//	});
//     
//     
//     
//	$("#wheel1").colorWheel1({
//		// controlsSelector: '.wheel-controls',
//		wheelWidth: 600,
//		pickedColorLables: ['Dress color 1', 'Dress color 2'],
//		initialColors: ['', ''],
//		addInputs: true,
//		addLabels: true,
//		enableKeyboardShortcuts: true,
//		pickContElement: 'div',
//		pickLabelElement: 'strong',
//		pickTextElement: 'span',
//		pickItemTitleText: 'Click to change this color'
//	});


   /* color picker ends */

    /* Save other user account for current user. */
    $("#anotheraccount").click(function(){
        var anotherUserEmail = $("#anotheruser").val(),
            password         = $("#anotherpassword").val(),
            confpass         = $('#cnfanotherpassword').val();
        var errors = { 'email_error': anotherUserEmail,'pass_error': password,'cnfpass_error' : confpass};

        if(anotherUserEmail && password && confpass){
            var verifyemail = checkEmail(anotherUserEmail);
            if(verifyemail == false){
                $(".email_invalid").show();
                $(".email_invalid").fadeOut(3000);    
            }

            if(password != confpass){
                $(".pass_invalid").show();
                $(".pass_invalid").fadeOut(3000);                     
            }

            if(verifyemail == true && password == confpass){
                $.ajax({
                    type : 'POST',
                    url  :  "referer-account", 
                    data : { otheruser : anotherUserEmail , otherpassword : password  }, 
                    success: function(response) {
                        console.log("jsonObj", response);                 
                        if(response.resp == 'success'){
                           $(".show-msg").html(response.message);
                        }else{
                           $(".show-msg").html(response.message);
                        }
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
            }
        }else{
            
            $.each(errors, function(key,value){
                if(value == ""){
                    $("."+key).show();
                    $("."+key).fadeOut(3000);              
                }else if(value){

                    if(key == 'email_error'){
                        var verifyemail = checkEmail(anotherUserEmail);
                        if(verifyemail == false){
                            $(".email_invalid").show();
                            $(".email_invalid").fadeOut(3000);                           
                        }

                    }
                }
            });
        }

    });

    /* custom validation for email address. */

    function checkEmail(email) {
        var regularExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regularExp.test(email);
    }
    
 // This is the simple bit of jquery to duplicate the hidden field to subfile
        $('#image_file').change(function(){
            $('#subfile').val($(this).val());
        }); 

    /* Save Gmail Contacts */
        $("#gmailcontacts").click(function(e){
	    $(".step2").removeClass('hide');
            e.preventDefault();   
            $("#gcontact").ajaxForm({
                success: function(data){
		    $(".step2").addClass('hide');
                    $('html, body').animate({ scrollTop: $(document).height() - $(window).height() }, 1000, function() {
                        $(this).animate({ scrollTop: 0 }, 1000);
                    });

               $("#final_message").html('<div class="alert alert-success" id="notification">'+ data.message +'&nbsp;&nbsp;<a href="javascript:void(0);" class="btn btn-default" id="close_window" >Close Window</a></div>');
                    $("a#close_window").click(function(){
                        window.opener = self;
                        window.close();
                    });
                }    
            }).submit();
            
        });



     $('#vendor_form').bootstrapValidator({

          submitHandler: function(validator, form, submitButton) {
               var category = $("#category").val();
               var country = $("#city").val();
               var email = $("#email").val();
     
               $.cookie("category", category);
               $.cookie("country", country);
               $.cookie("email", email);
     
               var opts = {
                    success: function(data) {
                         if($.trim(data.resp) == 'success'){
     
                             $('#error').html(data.message);
                             setTimeout(function(){
                            $('#error').hide();
                             },2000);
                         }
                         
                         else {
                            $("#content1").show();
                         }
                         
                    }
               };
               $(form).ajaxSubmit(opts);
          },


        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },

        fields: {
           category: {
                validators: {
                    notEmpty: {
                        message: 'The category is required and can\'t be empty'
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: 'The location is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 3,
                        //max: 30,
                        message: 'The location must be more than 3 characters long'
                    },
                    /*regexp: {
                        regexp: /^[a-zA-Z_,0-9\.]+$/,
                        message: 'The location can only consist of alphabetical, number, comma, dot and underscore'
                    } */ 
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and can\'t be empty'
                    },
                    emailAddress: {
                             message: 'The input is not a valid email address'
                                 }
                }   
            }
        }
     });   
   
     $('#vendor_account_form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                
                identical: {
                        field: 're_enter_password',
                        message: 'The new password and its confirm are not the same'
                    }
              }
            },
            re_enter_password: {
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The new password and its confirm are not the same'
                    }
                }
            }
        }
     });

     $(".rating_section").find('.rate_plan .plan_rest').each(function(index){
          $(this).find('ul.plan_details li.showPlan1').each(function(){
               $(this).hide();
          });
          $(this).find('a.showPlanDetails1').click(function(e){
               e.preventDefault();
               $(".plan_rest").find('ul.plan_details li.showPlan1').each(function(){
                    $(this).slideToggle( "slow", function() {
                    // Animation complete.
                    });
               });
          });
        });
     //$("#tabs").tabs();

	//Password Activation
	     $('.password_activate').bootstrapValidator({
              submitHandler: function(validator, form, submitButton) {
			 var opts = {
                                success: function(data) {
                                	console.log(data.resp);
					if(data.resp == 'success'){
						$('#password-reset').show();
						$('#password-reset').html(data.message);
						setTimeout(function(){
							window.location.href = baseUrl;
						}, 2000);
					}
				
                                }
                            };
                            $(form).ajaxSubmit(opts);
                },
		
						

                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                fields: {
                    email: {
                        trigger: 'blur', 
                        validators: {
                            notEmpty: {
                                message: 'The email address is required and can\'t be empty'
                            },
                            emailAddress: {
                                message: 'The input is not a valid email address'
                            }
                        }
                    }

                }
            });

	//request callback guest value check at home
	     $(document).ready(function () {
	     $("#guest").keypress(function(e){
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				$("#guest-check").html("Digits Only").show().fadeOut("slow");
				return false;
			}
		});
	     });
});


