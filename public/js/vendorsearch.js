jQuery(document).ready(function($){
    var process = true;
    $("form#vendor_search").find('input, checkbox, select').each(function(index){
       
        $(this).on({            
            change: function(){
                submit_search_form();
		   setTimeout(function(){
                     		locat();
                    },5000);

            },
            keyup: function(){                
                submit_search_form();
		 setTimeout(function(){
                     		locat();
                    },5000);
            }            
        });
        function locat(){ 
	        jeoquery.defaultData.userName = 'tompi';
                $(".city_home").jeoCityAutoComplete({callback: function(city) { if (console) console.log(city);}});
	    
	}
        function submit_search_form() {
              
                if ( process ) {
                    process = false;
                    $('.search-content').html("loading data....wait").promise().done(function(){
                        $("#vendor_search").ajaxForm({
                            target: ".search-content",
                            success: function(data){
                                
				   //@ Dinesh request to show Quotes form
				    $("a.callback_quote").click(function(e){
				       var dateToday = new Date();
					 $('.appointmentDate').datepicker('setDate', null);
					 $(".appointmentDate").datepicker("destroy");
					 $('.hasDatePickerWithCal').datepicker('setDate', null);
					 $(".hasDatePickerWithCal").datepicker("destroy");
				     	$(".appointmentDate").datepicker({
						dateFormat: 'MM dd, yy',
						minDate: dateToday,
						minDate: 0,
						changeMonth: true,
						changeYear: true,
						onSelect: function(selectedDate) {
						    $(this).next().val(selectedDate);
						} 
					});
		
					$( ".hasDatePickerWithCal" ).datepicker({
						showOn: "button",
						buttonImage: '/images/calender_icon.png',
						buttonImageOnly: true,
						dateFormat: 'MM dd, yy',
						changeMonth: true,
						changeYear: true,

						beforeShow: function (input, inst) {
			
							var changedate = 0;

						   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
						}
					});
				 	e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks        
					var id = $(e.target).attr('data-key');
					var key = $("#"+id).attr('data-check');
					$("#rqstipt"+key).hide();
					$("#cellno"+key).show();
					$("#radio4_"+key).prop('checked', true);
					$("#"+id).find('.celno').attr('required', true);
					$("#"+id).find('.stepExample1').removeAttr('required');
					$("#"+id).find('.stepExample2').removeAttr('required');
					$("#"+id).find('.appointmentDate').removeAttr('required');
					$("#"+id).show("slow");
				    
				    });
				    
				    //@ jeevan request handler for appointments with public vendors
				    $("a.request_appointment").click(function(e){
					       e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks
						  var dateToday = new Date();
						 $('.appointmentDate').datepicker('setDate', null);
						 $(".appointmentDate").datepicker("destroy");
						 $('.hasDatePickerWithCal').datepicker('setDate', null);
						 $(".hasDatePickerWithCal").datepicker("destroy");
					     	$(".appointmentDate").datepicker({
							dateFormat: 'MM dd, yy',
							minDate: dateToday,
							minDate: 0,
							changeMonth: true,
							changeYear: true,
							onSelect: function(selectedDate) {
							    $(this).next().val(selectedDate);
							}
						});
		
						$( ".hasDatePickerWithCal" ).datepicker({
							showOn: "button",
							buttonImage: '/images/calender_icon.png',
							buttonImageOnly: true,
							dateFormat: 'MM dd, yy',
							changeMonth: true,
							changeYear: true,
							beforeShow: function (input, inst) {
							    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
							    if(dd){
							    	var changedate = dd ;
							   }else{
								var changedate = 0;
							   }
							    $(".hasDatePickerWithCal").datepicker("option", { minDate: changedate });
							}
						});
					var id = $(e.target).attr('data-key');
					var key = $("#"+id).attr('data-check');
					$("#rqstipt"+key).show();
					$("#cellno"+key).hide();
					$("#radio5_"+key).prop('checked', true);
					$("#"+id).find('.celno').removeAttr('required');
					$("#"+id).find('.stepExample1').attr('required', true);
					$("#"+id).find('.stepExample2').attr('required', true);
					$("#"+id).find('.appointmentDate').attr('required', true);
					$("#"+id).show("slow");
				    });
    $('input:radio[name="radiog_dark"]').change(function(e){
        e.preventDefault();
        if ($(this).val() == "quote") {
	$('.hasDatePickerWithCal').datepicker('setDate', null);
	$(".hasDatePickerWithCal").datepicker("destroy");                  
        $( ".hasDatePickerWithCal" ).datepicker({
                showOn: "button",
                buttonImage: '/images/calender_icon.png',
                buttonImageOnly: true,
                dateFormat: 'MM dd, yy',
                changeMonth: true,
                changeYear: true,

		beforeShow: function (input, inst) {
		   var changedate = 0;
                   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
                }
	});
            var Cl1 = $(e.target).next().closest( "div" ).next().attr('id');
            var hid = $(e.target).parent().parent().next().find('.rqstipt-width').attr('id');
            $("#"+Cl1).show();
            $("#"+hid).hide();
        } 
        else {
	
	     $('.hasDatePickerWithCal').datepicker('setDate', null);
	     $(".hasDatePickerWithCal").datepicker("destroy");
		$( ".hasDatePickerWithCal" ).datepicker({
		        showOn: "button",
		        buttonImage: '/images/calender_icon.png',
		        buttonImageOnly: true,
		        dateFormat: 'MM dd, yy',
		        changeMonth: true,
		        changeYear: true,

			beforeShow: function (input, inst) {
			    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
			    if(dd){
			    	var changedate = dd ;
			   }else{
				var changedate = 0;
			   }
		           $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
		        }
		});
		 $('.appointmentDate').datepicker('setDate', null);
	         $(".appointmentDate").datepicker("destroy");
	        var dateToday = new Date();
     		$(".appointmentDate").datepicker({
			dateFormat: 'MM dd, yy',
			minDate: dateToday,
			minDate: 0,
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
		            $(this).next().val(selectedDate);
		        }
		});   
            var Cl = $(e.target).next().closest( "div" ).next().attr('id');
            var hid1 = $(e.target).parent().parent().parent().find('.cell').attr('id');
            $("#"+Cl).show();
            $("#"+hid1).hide();
        } 
    });    
				process = true;
                            }    
                        }).submit(); 
                    });
                    
                }        
        }
        
    });
    
});
