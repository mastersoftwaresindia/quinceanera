jQuery(document).ready(function($){
    
    //@ Dinesh request to show Quotes form
    $("a.callback_quote,#Requestcallvendorlink").click(function(e){
        var dateToday = new Date();
	 $('.appointmentDate').datepicker('setDate', null);
         $(".appointmentDate").datepicker("destroy");
	 $('.hasDatePickerWithCal').datepicker('setDate', null);
         $(".hasDatePickerWithCal").datepicker("destroy");
     	$(".appointmentDate").datepicker({
		dateFormat: 'MM dd, yy',
		minDate: dateToday,
		minDate: 0,
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
                    $(this).next().val(selectedDate);
                } 
	});
        
        $( ".hasDatePickerWithCal" ).datepicker({
                showOn: "button",
                buttonImage: 'http://'+document.location.hostname + '/za/public/images/calender_icon.png',
                buttonImageOnly: true,
                dateFormat: 'MM dd, yy',
                changeMonth: true,
                changeYear: true,

		beforeShow: function (input, inst) {
			
			var changedate = 0;

                   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
                }
	});
 	e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks        
        var id = $(e.target).attr('data-key');
        var key = $("#"+id).attr('data-check');
        $("#rqstipt"+key).hide();
        $("#cellno"+key).show();
        $("#radio4_"+key).prop('checked', true);
        $("#"+id).find('.celno').attr('required', true);
        $("#"+id).find('.stepExample1').removeAttr('required');
        $("#"+id).find('.stepExample2').removeAttr('required');
        $("#"+id).find('.appointmentDate').removeAttr('required');
        $("#"+id).show("slow");
    
    });
    
    //@Dinesh close Quote form
    $("a.close-sld-cntn").click(function(e){
        var id = $(e.target).parent().attr('id');
        $("#"+id).hide("slow");
    });
    
    /** script for 'US formatization for phone number' : Starts **/
       $("input[name=cell_no]").mask("999-999-9999",{placeholder:" "});
    	$('.stepExample1').timepicker({ 'step': 15 });
	$('.stepExample2').timepicker({ 'step': 60 });

        
    //Save Quote form data

        $(".form-quote").click(function(e){
            var dataId = $(e.target).parent().parent().parent().parent().attr('data-check');
        
            $("#quoteform"+dataId).ajaxForm({

                success: function(data){

                    $("#call_"+dataId).hide('slow');
                    $(".sucs-ms-srch").show();
                    $(".sucs-ms-srch").html(data.message);
                    $("#quoteform"+dataId)[0].reset();
		    $('.chosen-location').val("").trigger('chosen:updated');
                    setTimeout(function(){
                       $('.sucs-ms-srch').hide('slow');
                    },3000);
                    
                },
                error: function(){
                    
                }
              });
        });
            //Save Quote form data for vendor home 
         $(".form-quote1").click(function(e){
            var dataId = 1 ;

            $("#formcallbackquote").ajaxForm({

                success: function(data){
                    $("#call_"+dataId).hide('slow');
                    $(".sucs-ms-srching").show();
                    $(".sucs-ms-srching").html(data.message);
                    $("#formcallbackquote")[0].reset();
            $('.chosen-location').val("").trigger('chosen:updated');
                    setTimeout(function(){
                       $('.sucs-ms-srching').hide('slow');
                    },5000);
                    
                },
                error: function(){
                    
                }
              });
        });
    
    $('input:radio[name="radiog_dark"]').change(function(e){
        e.preventDefault();
        if ($(this).val() == "quote") {
	$('.hasDatePickerWithCal').datepicker('setDate', null);
	$(".hasDatePickerWithCal").datepicker("destroy");                  
        $( ".hasDatePickerWithCal" ).datepicker({
                showOn: "button",
                buttonImage: 'http://'+document.location.hostname + '/za/public/images/calender_icon.png',
                buttonImageOnly: true,
                dateFormat: 'MM dd, yy',
                changeMonth: true,
                changeYear: true,

		beforeShow: function (input, inst) {
		   var changedate = 0;
                   $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
                }
	});
            var Cl1 = $(e.target).next().closest( "div" ).next().attr('id');
            var hid = $(e.target).parent().parent().next().find('.rqstipt-width').attr('id');
            $("#"+Cl1).show();
            $("#"+hid).hide();
        } 
        else {
	
	     $('.hasDatePickerWithCal').datepicker('setDate', null);
	     $(".hasDatePickerWithCal").datepicker("destroy");
		$( ".hasDatePickerWithCal" ).datepicker({
		        showOn: "button",
		        buttonImage: 'http://'+document.location.hostname + '/za/public/images/calender_icon.png',
		        buttonImageOnly: true,
		        dateFormat: 'MM dd, yy',
		        changeMonth: true,
		        changeYear: true,

			beforeShow: function (input, inst) {
			    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
			    if(dd){
			    	var changedate = dd ;
			   }else{
				var changedate = 0;
			   }
		           $(".hasDatePickerWithCal").datepicker("option", { minDate:changedate });
		        }
		});
		 $('.appointmentDate').datepicker('setDate', null);
	         $(".appointmentDate").datepicker("destroy");
	        var dateToday = new Date();
     		$(".appointmentDate").datepicker({
			dateFormat: 'MM dd, yy',
			minDate: dateToday,
			minDate: 0,
			changeMonth: true,
			changeYear: true,
			onSelect: function(selectedDate) {
		            $(this).next().val(selectedDate);
		        }
		});   
            var Cl = $(e.target).next().closest( "div" ).next().attr('id');
            var hid1 = $(e.target).parent().parent().parent().find('.cell').attr('id');
            $("#"+Cl).show();
            $("#"+hid1).hide();
        } 
    });
    
    
    //@ jeevan request handler for appointments with public vendors
    $("a.request_appointment").click(function(e){
        e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks
	  var dateToday = new Date();
	 $('.appointmentDate').datepicker('setDate', null);
         $(".appointmentDate").datepicker("destroy");
	 $('.hasDatePickerWithCal').datepicker('setDate', null);
         $(".hasDatePickerWithCal").datepicker("destroy");
     	$(".appointmentDate").datepicker({
		dateFormat: 'MM dd, yy',
		minDate: dateToday,
		minDate: 0,
		changeMonth: true,
		changeYear: true,
		onSelect: function(selectedDate) {
                    $(this).next().val(selectedDate);
                }
	});
        
        $( ".hasDatePickerWithCal" ).datepicker({
                showOn: "button",
                buttonImage: 'http://'+document.location.hostname + '/za/public/images/calender_icon.png',
                buttonImageOnly: true,
                dateFormat: 'MM dd, yy',
                changeMonth: true,
                changeYear: true,
		beforeShow: function (input, inst) {
                    var dd = $(this).parent().parent().parent().find("input[name=date-com]").val();
		    if(dd){
		    	var changedate = dd ;
		   }else{
			var changedate = 0;
		   }
                    $(".hasDatePickerWithCal").datepicker("option", { minDate: changedate });
                }
	});
        var id = $(e.target).attr('data-key');
        var key = $("#"+id).attr('data-check');
        $("#rqstipt"+key).show();
        $("#cellno"+key).hide();
        $("#radio5_"+key).prop('checked', true);
        $("#"+id).find('.celno').removeAttr('required');
        $("#"+id).find('.stepExample1').attr('required', true);
        $("#"+id).find('.stepExample2').attr('required', true);
        $("#"+id).find('.appointmentDate').attr('required', true);
        $("#"+id).show("slow");
    });
    

    //@ jeevan request handler for appointments with public vendors
    $("a.request_favorite").click(function(e){
        e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks        
        var id = $(e.target).attr('data-key');
        var vendor = $("#"+id).find("input[name=vendor_member]").val();
        if( $(e.target).parent().attr('class') ) {
            var like = 0; 
        }else{
            var like = 1;
        }
        $.ajax({
            type: 'POST',
            url : '/users/operations/favourite/format/html',
            data: { vendor_member: vendor, terms: 'favorite', check : like }
        }).done(function(result) {
            if(result.image == 'add'){
                $(e.target).parent().addClass( 'favourite-icon' );
            }else {
                $(e.target).parent().removeClass( 'favourite-icon' );
            }
        });

    });
    
    //@ jeevan request handler for appointments with public vendors
    $("a.request_like").click(function(e){
        var id = $(e.target).attr('data-key');
        var cl = $(e.target).attr('id');
        var vendor = $("#"+id).find("input[name=vendor_member]").val();
        $("#target").load('/users/operations/likes/format/html?id='+ id + '&vendor='+ vendor , function(){   //load data from given location
            $(this).lightbox_me({
                centered: true,
                onLoad: function() {   
                    $('.close').click(function() {                        
                        $(this).trigger('close');    //event ->close                        
                         $(".rating-content").remove();
                    })                 
                    var that = this;
                    $('#vendor'+id).bootstrapValidator({
                        submitHandler: function(validator, form, submitButton) {
                            var star = $(form).find("input[name=star_rate]").val();
                            if ( star ) {
                            }else{
                                alert("Please give rating to this vendor.")
                                $(submitButton).prop("disabled", false);
                                return;    
                            }
                            
                            var opts = {
                                success: function(data) {
                                   console.log(data);
                                   $("#"+cl).html('Like [ '+ data.count +' ]');
                                   $(".lb_overlay").remove();
                                   $(".rate-model").hide('slow');                         
                                   $(".rating-content").remove();
                                }
                            };
                            $(form).ajaxSubmit(opts);
                        }                   
                    });
                    
                },
                closeClick: false,
                appearEase: "swing",                
            }); 
        });
        e.preventDefault(); //to prevent same popup appear multiple times on  multiple clicks        
    });

    $("a.request_appointment1").click(function(e){

        alert("clicked");
    });
});
