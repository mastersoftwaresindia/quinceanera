<?php

class Application_Form_User extends Zend_Form
   {
       public function init()
       {
           // Set the method for the display form to POST
           $this->setMethod('post');

           
           // Add an email element
           $this->addElement('text', 'email', array(
               'label'      => 'Your email address:',
               'required'   => true,
               'filters'    => array('StringTrim'),
               'validators' => array('EmailAddress'),
           ));


        
           // Add an password element
           $this->addElement('text', 'password', array(
               'label'      => 'Your Password:',
               'required'   => true,
               'filters'    => array('StringTrim'),
              
           ));

          
           // Add an confirm password element
           $this->addElement('text', 'confirm_password', array(
               'label'      => 'Your confirm Password:',
               'required'   => true,
               'filters'    => array('StringTrim'),
               'validators' => array(array('Identical', true, 'password')),
               
               
             ));



           // Add the submit button
           $this->addElement('submit', 'createAccount', array(
               'ignore'   => true,
               'label'    => 'Sign User',
           ));


           // Add the comment element
           // $this->addElement('textarea', 'comment', array(
           //     'label'      => 'Please Comment:',
           //     'required'   => true,
           //     'validators' => array(
           //         array('validator' => 'StringLength', 'options' => array(0, 20))
           //         )
           // ));

           // Add a captcha
           $this->addElement('captcha', 'captcha', array(
               'label'      => 'Please enter the 5 letters displayed below:',
               'required'   => true,
               'captcha'    => array(
                   'captcha' => 'Figlet',
                   'wordLen' => 5,
                   'timeout' => 300
               )
           ));


           // And finally add some CSRF protection
           $this->addElement('hash', 'csrf', array(
               'ignore' => true,
           ));
       }
   }
