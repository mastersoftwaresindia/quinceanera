<?php

class Admin_MediaUploaderController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('vup', 'html')
                    ->addActionContext('album', 'html')
                    ->addActionContext('del', 'html')
		    ->addActionContext('delalbum', 'html')
                    ->addActionContext('delimage', 'html')
                    ->initContext();
    }

    public function indexAction() {
        // action body
        
        $request = new Zend_Controller_Request_Http;
        //get default session namespace
	
        $sess = new Zend_Session_Namespace('Default');
       
	if ( $request->isGet() ) {
            $db=Zend_Registry::get("db");
            $user    = $request->get('user');
            //$type    = $request->get('type');
            $section = $request->get('section');
            $result = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $user, $section ), 2);
            $this->view->data  = array('method'=>$this->getRequest()->getMethod(), 'data'=>$result); 
        }
        
        if ( $request->isPost() ) {
            $iWidth = (int)$_POST['ww']; // desired image result dimensions
            $iHeight= (int)$_POST['hh'];
	    $sess = $_POST['user'];
            $iJpgQuality = 100;         
            try {
                $db=Zend_Registry::get("db");
                //print "Jeevan1"; 
                if ($_FILES) {
                    //print "Jeevan1"; die;
                    if (! $_FILES['image_file']['error']) {
                        if (is_uploaded_file($_FILES['image_file']['tmp_name'])) {                            
                            if( $_POST['section'] ) {
                                $section = $_POST['section'];
                                // Change directory
                                chdir("media");
                                if (!file_exists(''.$sess.'/')) {
                                    mkdir(''.$sess.'/', 0777, true);
                                    chdir(''.$sess.'/');
                                } else {
                                    chdir(''.$sess.'/');
                                }                        
                                // new unique filename
                                $sTempFileName = ''.$_POST['section'].'_image_'.md5(time().rand());                        
                                // move uploaded file into cache folder
                                move_uploaded_file($_FILES['image_file']['tmp_name'], $sTempFileName);                        
                                // change file permission to 644
                                @chmod($sTempFileName, 0644);                        
                                if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
                                    $aSize = getimagesize($sTempFileName); // try to obtain image info
                                    if (!$aSize) {
                                        @unlink($sTempFileName);
                                        return;
                                    }
                                    // check for image type
                                    switch($aSize[2]) {
                                        case IMAGETYPE_JPEG:
                                            $sExt = '.jpg';
                                            // create a new image from file
                                            $vImg = @imagecreatefromjpeg($sTempFileName);
                                            break;
                                        case IMAGETYPE_PNG:
                                            $sExt = '.png';
                                            // create a new image from file
                                            $vImg = @imagecreatefrompng($sTempFileName);
                                            break;
                                        case IMAGETYPE_PNG:
                                            $sExt = '.jpeg';
                                            // create a new image from file
                                            $vImg = @imagecreatefromjpeg($sTempFileName);
                                            break;
                                        case IMAGETYPE_GIF:
                                            $sExt = '.gif';
                                            // create a new image from file
                                            $vImg = @imagecreatefromgif($sTempFileName);
                                            break;
                                        default:
                                            @unlink($sTempFileName);
                                            return;
                                    }
                                    // create a new true color image
                                    $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
                                    
                                    //@Jeevan [ replace black color in transparent image starts ]
                                    imagesavealpha($vDstImg, true);
                                    imagealphablending($vDstImg, false);
                                    $transparent = imagecolorallocatealpha($vDstImg, 0, 0, 0, 127);
                                    imagefill($vDstImg, 0, 0, $transparent);
                                    //@Jeevan [ replace black color in transparent image ends ]
                                    
                                    // copy and resize part of an image with resampling
                                    imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);    
                                    // define a result image filename
                                    $sResultFileName = $sTempFileName . $sExt;
                                    // output image to file
                                    imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
                                    @unlink($sTempFileName);
                                    $sImage = $sResultFileName;
				    $multi =  (isset($_POST['multi'])) ? $_POST['multi'] : 0;
				    $imageTitle = (isset($_POST['title'])) ? $_POST['title'] : '';
                                    $imageDesc = (isset($_POST['description'])) ? $_POST['description'] : '';
                                    $imageGuid = (isset($_POST['guid'])) ? $_POST['guid'] : '';
                                    $album_name = (isset($_POST['album_name'])) ? $_POST['album_name'] : '';
                                    $album_description = (isset($_POST['album_description'])) ? $_POST['album_description'] : '';
                                    if($album_name != 'none'){
                                    //@Jeevan saving media info in database start 
                                    $record = array(
                                        'owner'    => $sess,
                                        'type' =>$sExt,
                                        'title' => $imageTitle,
                                        'guid' => $imageGuid,
                                        'section' =>$section,                                       
                                        'path' =>$sImage,
                                        'description' => $imageDesc,
                                        'multi' => $multi,
                                        'added_on' => date("Y-m-d H:i:s"),
                                        'updated_on' => date("Y-m-d H:i:s"),
                                        'album_id' => $album_name,

                                    );
                                    $result = $db->fetchAll("SELECT * FROM media where owner=? and section=?", array($sess, $section), 2);                                    
                                    if ( $result ) {
					if($section=='site-logo' || $section=='user-avatar') {
                                            $n = $db->update('media', $record, 'owner="'.$sess.'" and section="'.$section.'"');
                                        } else {
                                            $n = $db->insert('media', $record);
					    $lastInsertId = $db->lastInsertId();
                                        }
                                    } else {
                                        $n = $db->insert('media', $record);
					 $lastInsertId = $db->lastInsertId();
                                    }}else{
					$record = array(
                                        'owner'    => $sess,
                                        'type' =>$sExt,
                                        'title' => $imageTitle,
                                        'guid' => $imageGuid,
                                        'section' =>'vendor_image',                                       
                                        'path' =>$sImage,
                                        'description' => $imageDesc,
                                        'multi' => $multi,
                                        'added_on' => date("Y-m-d H:i:s"),
                                        'updated_on' => date("Y-m-d H:i:s"),
                                      

                                    );
                                    $result = $db->fetchAll("SELECT * FROM media where owner=? and section=?", array($sess, $section), 2);                                    
                                    if ( $result ) {
					if($section=='site-logo' || $section=='user-avatar') {
                                            $n = $db->update('media', $record, 'owner="'.$sess.'" and section="'.$section.'"');
                                        } else {
                                            $n = $db->insert('media', $record);
					    $lastInsertId = $db->lastInsertId();
                                        }
                                    } else {
                                        $n = $db->insert('media', $record);
					 $lastInsertId = $db->lastInsertId();
                                    }
				    }
                                    //@Jeevan saving media info in database ends                                    
                                    
                                    // Define extreme image cropper function  
                                    function extreme_cropper($image, $nw, $count=0) {
                                        list($section, $owner, $img) = explode("___", $image);
                                        list($name, $ext) = explode(".", $img);
                                        
                                        $uploadedfile = PUBLIC_URI.'/media/'.$owner.'/'.$img; 
                                        // Create an Image from it so we can do the resize
                                        
                                        $src = imagecreatefromjpeg($uploadedfile);
                     
                                        // Capture the original size of the uploaded image
                                        list($width,$height)=getimagesize($uploadedfile);
                     
                                        // For our purposes, I have resized the image to be
                                        // 600 pixels wide, and maintain the original aspect
                                        // ratio. This prevents the image from being "stretched"
                                        // or "squashed". If you prefer some max width other than
                                        // 600, simply change the $newwidth variable
                                        $newwidth=$nw;
                                        $newheight=($height/$width)*$nw;
                                        $tmp=imagecreatetruecolor($newwidth,$newheight);
                                        
                                        imagesavealpha($tmp, true);
                                        imagealphablending($tmp, false);
                                        $transparent = imagecolorallocatealpha($tmp, 0, 0, 0, 127);
                                        imagefill($tmp, 0, 0, $transparent);
                                        
                                        // this line actually does the image resizing, copying from the original
                                        // image into the $tmp image
                                        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
                        
                                        // now write the resized image to disk. I have assumed that you want the
                                        // resized, uploaded image file to reside in the ./images subdirectory.
                                        $filename = $section."_".$owner."_".$count."_".$nw."x".$newheight.".".$ext;
                                        imagejpeg($tmp,$filename,100);
                        
                                        imagedestroy($src);
                                        imagedestroy($tmp); // NOTE: PHP will clean up the temp file it created when the request
                                        // has completed.         
                                    }
                                    // End extreme image cropper function  
                                    
                                    // create custom image string 
                                    $image = ''.$_POST['section'].'___'.$sess.'___'.$sImage;
                                    
                                    /* Get media data from database */
                                    // @jeevan starts
                                    list($usersection, $user, $userimage) = explode("___", $image);
                                    $result = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $user, $usersection ), 2);
                                    if( $result ) {
                                        if($usersection=='user-avatar') {
                                            extreme_cropper($image,$nw=35);                
                                            extreme_cropper($image,$nw=51);
                                            extreme_cropper($image,$nw=64);
                                            extreme_cropper($image,$nw=250);
                                        }
                                        if($usersection=='slider') {
                                            //$count = count($result);
					    $count = $lastInsertId;
                                            extreme_cropper($image,$nw=1680,$count);
                                        }
					if($usersection=='advertisement') {
                                            //$count = count($result);
					    $count = $lastInsertId;
                                            extreme_cropper($image,$nw=280,$count);
                                        }
					if($usersection=='vendorslider') {
					    $count = $lastInsertId;
                                            extreme_cropper($image,$nw=1680,$count);
                                        }
                                        if($usersection=='venueidea') {
                                            //$count = count($result);
					    $count = $lastInsertId;
                                            extreme_cropper($image,$nw=400,$count);
                                        }
                                        if($usersection=='vendorgallery') {
                                            //$count = count($result);
					    $count = $lastInsertId;
                                            extreme_cropper($image,$nw=960,$count);
                                        }
                                        if($usersection=='site-logo') {
                                            extreme_cropper($image,$nw=600);
                                        }                    
                                    }
                                    //@jeevan ends
                                    // This is the temporary file created by PHP''.$_POST['section'].'/'.$sess->user.'/'.$sImage                                    
                                    $data = array('method'=>$this->getRequest()->getMethod(), 'user'=>$result[0]['owner'], 'type'=>$result[0]['type'], 'data'=>''.$sess.'___'.$sImage);
                                    $this->view->data  = $data;
                                }
                                
                            } else {
                                    $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>'Image section is not defined !!!');
                                    $this->view->data  = $data;   
                            }
                        }
                    } else {
                        $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>$_FILES['image_file']['error']);
                        $this->view->data  = $data; 
                    }
                } else {
                        $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>'select a file to upload !');
                        $this->view->data  = $data;
                }
                
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                print $e;
        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                print $e;
            }
        }
    }


public function vupAction() {

    $request = new Zend_Controller_Request_Http;
    //get default session namespace
    Zend_Session::rememberMe(604800); // Week
    $sess = new Zend_Session_Namespace('UserSession');
    $db=Zend_Registry::get("db");
    if($request->isPost()) {

    // print_r($_POST);die;

        if($_POST['video_name'] && $_POST['video_description'] && $_POST['guid']) {
	
            function videoType($guid) {

                if (strpos($guid, 'youtu.be') > 0) {
                    return 'youtube';
                } elseif (strpos($guid, 'youtube') > 0) {
                    return 'youtube';
                }
                 elseif (strpos($guid, 'vimeo') > 0) {
                    return 'vimeo';
                } else {
                    return false;
                }
            }
            if(videoType($_POST['guid'])) {
                
                $video_data = array('video_name'   => $_POST['video_name'],            
                            'video_description'    => $_POST['video_description'],
                            'guid'      => $_POST['guid'],
                            'owner' => $_POST['vendor_id'],
                            'type' => 'video',
                            'section'=> 'vendorvideoimage',
			    'added_on' => date("Y-m-d H:i:s"),
                            'updated_on' => date("Y-m-d H:i:s")
                            );

                $n = $db->insert('media', $video_data);
                if($n){

                    $this->view->data = array('data'=>"Your Video has been uploaded successfully .It will be shown in videos section on Admin Approval within 24 hrs.");


                } else{


                    $this->view->data = array('data'=>"Your Video has not been uploaded ");
                }

            } else {

                    $this->view->data = array('data'=>"Your Video is invalid");

            }

        }   

        else{

          $this->view->data = array('data'=>"Please fill all the required fields.");


        }     

      }
}

  public function albumAction() {

    $request = new Zend_Controller_Request_Http;
    //get default session namespace
    Zend_Session::rememberMe(604800); // Week
    $sess = new Zend_Session_Namespace('UserSession');
    $db=Zend_Registry::get("db");

    if($request->isPost()) {

        //print_r($_POST);die;
        $user_id= $_POST['vendor_id'];
        $album_name = $_POST['album_name'];


        if($_POST['album_name'] && $_POST['album_description']) {

          $albums = $db->fetchAll('select * from albums where user_id=? and name=?', array($user_id, $album_name), 2);
         // print_r($albums);die;
          
           if(!$albums){
   
            $album_data = array('name'        => $_POST['album_name'],            
                                'description' => $_POST['album_description'],
                                'user_id'     => $_POST['vendor_id'],
                                );

                $n = $db->insert('albums', $album_data);
                if($n){

                    $this->view->data = array('data'=>"Your Album has been added successfully");

                } else{


                    $this->view->data = array('data'=>"Your Album has not been added ");
                }

            }

            else {

                 $this->view->data = array('data'=>"Album with this name already exists. ");

              }

           
        }   

        else{

          $this->view->data = array('data'=>"Please fill all the required fields.");


        }     

      }
}

    // @mssjeevan support profile
    public function delAction() {
 
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('media', 'id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Order deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'package', 'action'=>'index');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete order, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }

    public function delalbumAction() {
       
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");

                if( $id = $request->get('id') ) {

                    $n = $db->delete('albums', 'id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Album deleted successfully !');
                        //$urlOptions = array('module'=>'admin', 'controller'=>'package', 'action'=>'index');
                        //$this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete album, kindly retry !');
                     }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
    
    }

    public function delimageAction() {     
        if($this->getRequest()->isGET()) {  
            $request = new Zend_Controller_Request_Http;
            // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $image = $db->fetchAll('select * from media where id =?', array($id));
                    $n = $db->delete('media', 'id = '.$id.'');
                    if ( $n ) {                        
                        // echo "file removed"; @ jeevan first find out all files matching to a pattern in the media folder of user and delete one by one.
                        foreach(glob('./media/'.$image[0]->owner.'/'.$image[0]->section.'_'.$image[0]->owner.'_'.$image[0]->id.'_*x*') as $file) {
                            if(is_file($file))
                                //echo "file exists";
                                @unlink($file);
                        }
			$this->view->data = array('data'=>'Image deleted successfully !');
                        //$urlOptions = array('module'=>'admin', 'controller'=>'package', 'action'=>'index');
                        //$this->_helper->redirector->gotoRoute($urlOptions);                        
                    } else {
                        $this->view->data = array('data'=>'Unable to delete image, kindly retry !');
                     }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
    
    }


}



