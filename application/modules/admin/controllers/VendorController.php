<?php

class Admin_VendorController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
	$this->_helper->layout->setLayout('admin');
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('view', 'html')
	            ->addActionContext('edit', 'html')
		    ->addActionContext('delete', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        $request = new Zend_Controller_Request_Http;
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	
	// check if user is not logged in
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if ( $request->isGet() ) {
            try {
                
                $db=Zend_Registry::get("db");
                $result = $db->fetchAll("SELECT v.first_name,v.last_name,v.user_id,v.id as vid,v.email,status,p.id as pid,p.name,v.plan_end_date FROM user AS u LEFT JOIN vendor AS v ON u.id = v.user_id LEFT JOIN plans AS p ON v.plan_id = p.id WHERE u.role =? ORDER BY u.id DESC ", array(2), 2);
		
                $media = $db->fetchAll("SELECT * FROM media", array(), 2);
                if ( !$result ) {
                    print("No User found !!!");
                } else {
                    $this->view->data = array('vendor'=>$result,'media'=>$media);
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }   
            
        }      
        
    }
    
    public function viewAction()
    {
        $request = new Zend_Controller_Request_Http;
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	
	// check if user is not logged in
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        //@jeevan normal get request from the buddy manager
        if ( $request->isGet() ) {
            try {                
                if($request->get('id') && $request->get('type') && $request->get('case')) {
                    echo $id = $request->get('id');
                    echo $type = $request->get('type');
                    echo $case = $request->get('case');
                    
                    $db=Zend_Registry::get("db");
                    $result = $db->fetchAll("SELECT * FROM user AS u INNER JOIN vendor AS v ON u.id = v.user_id WHERE v.user_id =?", array($id), 2);
                    $media = $db->fetchAll("SELECT * FROM media where owner=?", array($id), 2);
                    if ( !$result ) {
                        print("No User found !!!");
                    } else {
                        $this->view->data = array('vendor'=>$result,'media'=>$media);
                    }
                }                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }           
        }
        
    }
    
    public function editAction()
    {
        $db=Zend_Registry::get("db");
        $request = new Zend_Controller_Request_Http;
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	
	// check if user is not logged in
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        //@jeevan normal get request from the buddy manager
        if ( $request->isGet() ) {
            try {                
                if($request->get('id')) {
                    echo $id = $request->get('id');                    
                    $db=Zend_Registry::get("db");
                    $result = $db->fetchAll("SELECT * FROM user AS u INNER JOIN vendor AS v ON u.id = v.user_id WHERE v.user_id =?", array($id), 2);
		 
                    $media = $db->fetchAll("SELECT * FROM media where owner=?", array($id), 2);
                    if ( !$result ) {
                        print("No User found !!!");
                    } else {
                        $this->view->data = array('vendor'=>$result,'media'=>$media);
                    }
                }                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }           
        }
        
        //@jeevan normal post request from the buddy manager
        if ( $request->isPost() ) {
            try {
                $stat = $this->getRequest()->getPost('status', null);
                $id = $this->getRequest()->getPost('id', null);
		 $data1 = array(
                                'first_name'  => $_POST['first_name'],
                                'last_name' => $_POST['last_name'],
				'about' => $_POST['about'],
				'address' => $_POST['address'],
				'zipcode' => $_POST['pincode'],
				'city' => $_POST['city'],
				'state' => $_POST['state'],
				'phone' => (int)$_POST['phonenumber'],
				'company_name' => $_POST['company_name'],
				'website' => $_POST['website'],
				'email' => $_POST['email'],
				'fb_url' => $_POST['fb_url'],
				'twitter_url' => $_POST['twitter_url'],
				'youtube_url' => $_POST['youtube_url'],
				'instagram_url' => $_POST['instagram_url'],
				'pinterest_url' => $_POST['pinterest_url'],
				'plan_end_date' => $_POST['plan_end_date']
				
                            );
		
		    $n1 = $db->update('vendor', $data1,'user_id = '.$id.'');
		if($n1)
		{
		$urlOptions = array('module'=>'admin', 'controller'=>'vendor', 'action'=>'index');
                $this->_helper->redirector->gotoRoute($urlOptions);
		}
                $data = array(
                    'status' => $stat
                );
                $result = $db->fetchAll("SELECT * FROM user AS u INNER JOIN vendor AS v ON u.id = v.user_id WHERE v.user_id =?", array($id), 2);
                if ( $result ) {
		
                    $n = $db->update('user', $data, 'id = '.$id.'');
                    if ($n) {                    
			if($stat == 1){
			    	$status = 'Activated';
			}else{
				$status = 'Blocked';
			}
			Zend_Loader::loadClass('Zend_View');
			$html = new Zend_View();
			$html->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/email/');
			$html->assign('email', $result[0]['email']);
			$html->assign('status', $status);

			$mail = new Zend_Mail('utf-8');
		        $bodyText = $html->render('accountStatus.phtml');
			
			$mail->addTo($result[0]['email'], '');
			$mail->setSubject('Quinceanera Account Status Mail');
			$mail->setFrom('admin.quinceanera@gmail.com', 'Email From Quinceanera.');
			$mail->setBodyHtml($bodyText);		      
			$mail->send();

                        echo "<div id='notification' class='alert alert-danger'>";
                            echo "Account Status updated successfully.";
                        echo "</div>";
                    } else {
                        echo "<div id='notification' class='alert alert-danger'>";
                            echo mysql_error();
                        echo "</div>";
                    }
		    $data = $db->fetchAll("SELECT * FROM user where id=?", array($id), 2);
                    $this->view->data = array('buddies'=>$data);
		    	$urlOptions = array('module'=>'admin', 'controller'=>'vendor', 'action'=>'index');
                $this->_helper->redirector->gotoRoute($urlOptions);
                } else {                            
                    echo "<div id='notification' class='alert alert-danger'>";
                        echo "User not found !!!!";
                    echo "</div>";
                }                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
        
    }
    
    // @mssjeevan support profile
    public function delAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('user', 'id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Order deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'vendor', 'action'=>'index');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete order, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }


}

