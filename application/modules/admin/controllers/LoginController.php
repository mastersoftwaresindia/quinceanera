<?php

class Admin_LoginController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('adminlogin');
        $this->_redirector = $this->_helper->getHelper('Redirector');        
        
    }

    public function indexAction()
    {
        $request = new Zend_Controller_Request_Http;
        //get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( isset($sess->user ) ){
	    $urlOptions = array('module'=>'admin', 'controller'=>'index', 'action'=>'index');
	    $this->_helper->redirector->gotoRoute($urlOptions);
	}
	
        if ( $request->isGet() ) {
            
            //display login form
            
        }        
        if ( $request->isPost() ) {
            try {
                $email = $this->getRequest()->getPost('email', null);
                $password = md5($this->getRequest()->getPost('password', null));
                $db=Zend_Registry::get("db");
                $result = $db->fetchAll("SELECT * FROM user where email=? and password=? and admin=1", array($email,$password), 2);
                if ( !$result ) {
                    print("User with this email and password does not exist !");
                } else {
                    Zend_Session::rememberMe(604800); // Week
                    $sess = new Zend_Session_Namespace('Default');
                    $sess->user = $result[0]['id'];
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
        //check if user is logged in
	if( isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
    }


}

