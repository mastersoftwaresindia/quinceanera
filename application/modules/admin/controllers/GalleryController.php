<?php

class Admin_GalleryController extends Zend_Controller_Action
{

    public function init() {
        
        /* Initialize action controller here */
        $this->_helper->layout->setLayout('admin');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('index', 'html')
                    ->addActionContext('parent', 'html')
		    ->addActionContext('checkorder' , 'html')
		    ->addActionContext('media-status' , 'html')
                    ->initContext();
    }

    public function indexAction() {
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }        
        if($request->isGet()) {                
            // normal get method
            try{                
                $result = $db->fetchAll("select * from media where section=?", array('slider'), 2);
                if( $result ) {                    
                    $this->view->data = $result;                    
                } else {                    
                    $this->view->data = NULL;                    
                }                
                
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }        
                
        if($request->isPost()) {
            try{
                $data = array(
                    'post_title' => $_POST['post_title'],
                    'post_author' => $_POST['post_author'],
                    'post_date' => date("Y-m-d H:i:s"),
                    'post_date_gmt' => date("Y-m-d H:i:s"),
                    'post_content' => mysql_real_escape_string($_POST['post_content']),
                    'post_excerpt' => mysql_real_escape_string($_POST['post_excerpt']),
                    'post_status' => 'publish',
                    'comment_status' => 'open',
                    'ping_status' => 'open',
                    'post_password' => '123',
                    'post_name' => $_POST['post_title'],
                    'post_modified' => date("Y-m-d H:i:s"),
                    'post_modified_gmt' => date("Y-m-d H:i:s"),                
                    'post_content_filtered' => $_POST['post_author'],
                    'post_parent' => $_POST['parent'],
                    'guid' => '123',
                    'menu_order' => $_POST['menu_order'],
                    'menu_location' => $_POST['menu_location'],
                    'post_type' => $_POST['post_type'],
                    'post_mime_type' => 'default',
                );
                $n = $db->insert('pages', $data);
                if( $n ) {
                    
                } else {            
                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
    }
    
    public function parentAction() {
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $this->_redirector->gotoSimple('index', 'login' , null );
        }
        try{
            // get default values of pages from database
            $parent = $this->getRequest()->getPost('id', null);
            $result = $db->fetchAll('select * from pages where post_type=?', array($parent), 2);
            if( $result ) {         
                $this->view->data = $result;
            } else {
                $this->view->data = "0";            
            }
        } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
        } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        }
    }

    public function checkorderAction() {
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
        try{
            // get default values of pages from database
	    $order = $request->get('order');
            $result = $db->fetchAll('select * from pages where menu_order=?', array($order), 2);
            if( count($result) > 0) {         
		$this->_helper->json(
		    array( 'message' => 'success' ,
			   'resp'    => 'success'
		    )
	        );
	        return;
            } else {
                $this->_helper->json(
		    array( 'message' => 'error' ,
			   'resp'    => 'error'
		    )
	        );           
            }
        } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
        } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        }
    }
    
    // @mssjeevan support profile
    public function delAction() {
	
	// get default session namespace
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('Default');
	if( !isset($sess->user ) ){
            $urlOptions = array('module'=>'admin', 'controller'=>'login', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {  
            $request = new Zend_Controller_Request_Http;
            // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $image = $db->fetchAll('select * from media where id =?', array($id));
                    $n = $db->delete('media', 'id = '.$id.'');
                    if ( $n ) {                        
                        // echo "file removed"; @ jeevan first find out all files matching to a pattern in the media folder of user and delete one by one.
                        foreach(glob('./media/'.$image[0]->owner.'/'.$image[0]->section.'_'.$image[0]->owner.'_'.$image[0]->id.'_*x*') as $file) {
                            if(is_file($file))
                                //echo "file exists";
                                @unlink($file);
                        }
			$this->view->data = array('data'=>'Image deleted successfully !');
                        $urlOptions = array('module'=>'admin', 'controller'=>'gallery', 'action'=>'index');
                        $this->_helper->redirector->gotoRoute($urlOptions);                        
                    } else {
                        $this->view->data = array('data'=>'Unable to delete image, kindly retry !');
                     }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
    }
	
	 public function mediaStatusAction(){
        $db=Zend_Registry::get("db");
        try{
            $update = array('status'  =>  $_POST['flag']);
            $updateMedia = $db->update('media', $update, 'id ='. $_POST['id']);
            if( $updateMedia ){
                $this->_helper->json(
                    array('message' => 'Updated !!' ,
                        'resp' => 'success'
                    )
                );
            return;
            } else { 
                $this->_helper->json(
                array('message' => 'Not updated' ,
                      'resp' => 'error'
                )
                );
                return;
            }
        } catch (Zend_Db_Adapter_Exception $e) {
                // perhaps a failed login credential, or perhaps the RDBMS is not running
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
        } catch (Zend_Exception $e) {
                // perhaps factory() failed to load the specified Adapter class
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        }
    }
}

