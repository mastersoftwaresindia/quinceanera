<?php

class Users_ConversationController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
        //$this->_helper->layout->setLayout('user');
	$this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
    {
        // action body
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			   
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
    }

    public function viewAction()
    {
        // action body
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
     	if($this->getRequest()->isGET()) {
            if( isset($UserSession->userId) ){
                $request = Zend_Controller_Front::getInstance()->getRequest();
                $params = $request->getParams();
                $sql = 'SELECT * FROM user WHERE id = ?';
                $result = $db->fetchAll($sql, $UserSession->userId);
                $usersection = 'user-avatar';
                $id = $this->getRequest()->get('id');
                $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                $select = $db->select();
                
                $select->from(array('m' => 'messages'), array( 'id', 'by_from', 'sent_to', 'message', 'date', 'status', 'conv_id' ))
                       ->joinLeft(array('u' => 'user'), 'u.id = m.by_from WHERE m.conv_id ='.$id .' AND m.status in (0,1)', array('email', 'first_name', 'last_name'));
                $messages = $db->fetchAll($select);
                $messages = json_decode(json_encode($messages), true);
                if( $messages ){
                    $data = array(
                      'status'  => 1
                    );                       
                    $updateMessages = $db->update('messages', $data, 'conv_id ='.$params['id']. ' AND sent_to=' .$UserSession->userId. ' AND status = 0');
                }                      
                if($resultMedia){
                    $media = $resultMedia ;
                }else {
                    $media = '';
                }
                $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media, 'messages'=>$messages);
            }
	}
        
        if($this->getRequest()->isPOST()) {
	    
            try {
                // pretend this is a sophisticated database query
                
                $data = array(
                            'sent_to' => $_POST['sent_to'],
                            'by_from' => $_POST['by_from'],
                            'message' => $_POST['message'],
                            'conv_id' => $_POST['conv_id'],
                            'date'    => date("Y-m-d H:i:s"),
                        );		   
                $n = $db->insert('messages', $data);
                
                if($n) {
                    $this->_redirector->gotoSimple('view', 'conversation' , null , array('id' => $_POST['conv_id'], 'to' => $_POST['sent_to']) );
                }
                
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
	}
        
    }
    
    
    
    /* Messages Box (Sent) Action. */
    public function sentAction() {
	$db=Zend_Registry::get("db");
	$sess = new Zend_Session_Namespace('UserSession');
	if( !isset($sess->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        if($this->getRequest()->isGET()) {
            try {
                if( isset( $sess->userId ) ){
                    $result = $db->fetchAll("SELECT  a.* FROM    messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where by_from = ? AND status in (0,1) GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($sess->userId), 2);

                    $ids = array();
                    if( isset($result) ){
                        foreach($result as $key=> $data) {
                            $ids[] = $data['sent_to'];
                        }
                    }
		    if(!empty($ids)){
                    	$id = implode(",", $ids );

                	$user = $db->fetchAll("select * from user where id in ($id)", array($sess->userId), 2);
		    }else{
			$user =NULL;
			}

                    if( $result ) {                    
                        $this->view->data = array('messages'=>$result, 'user'=> $user);                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
	
    }
    
    /* Messages Box (trash) Action. */
    public function trashAction() {
        // action body
	
        $db=Zend_Registry::get("db");
	$sess = new Zend_Session_Namespace('UserSession');
	
        if( !isset($sess->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
	if($this->getRequest()->isGET()) {
            try {
	    
                if( isset( $sess->userId ) ){
                    $result = $db->fetchAll("SELECT * from messages where by_from = ? and status=3", array( $sess->userId ), 2);
		    if( $result ) {                    
                        $this->view->data = array( 'messages' => $result );                    
                    } else {                    
                        $this->view->data = NULL;                    
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }     
	}
    }
    
    
    
    public function delAction() {
	Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('UserSession');
	if( !isset($sess->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
	    // pretend this is a sophisticated database query
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n = $db->delete('messages', 'conv_id = '.$id.'');
                    
                    if ( $n ) {
                        $this->view->data = array('data'=>'Conversation deleted successfully !');
                        $urlOptions = array('module'=>'users', 'controller'=>'dashboard', 'action'=>'mailbox');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete conversation, kindly retry !');
                    }
                }                
            } catch (Exception $e ) {
                $this->view->data = array('data'=>$e);
            }
        }
	
    }
    
    
    public function delpermaAction() {
        Zend_Session::rememberMe(604800); // Week
        $sess = new Zend_Session_Namespace('UserSession');
        if($this->getRequest()->isGET()) {
            $request = new Zend_Controller_Request_Http;
            try{                
                $db=Zend_Registry::get("db");
                if( $id = $request->get('id') ) {
                    $n  = $db->delete('messages', 'id = '.$id);
                    if ( $n ) {
                        $this->view->data = array('data'=>'Message deleted successfully !');
                        $urlOptions = array('module'=>'users', 'controller'=>'conversation', 'action'=>'trash');
                        $this->_helper->redirector->gotoRoute($urlOptions);
                    } else {
                        $this->view->data = array('data'=>'Unable to delete Message, kindly retry !');
                    }
                }                
            } catch ( Exception $e ) {
                $this->view->data = array('data' => $e);
            }
        }
    }
    
    

}

