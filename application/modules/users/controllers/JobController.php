<?php

class Users_JobController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('new', 'html')
		    ->addActionContext('edit', 'html')
		    ->addActionContext('operationonapplier', 'html')
                    ->addActionContext('del', 'html')
		    ->addActionContext('jobapplication', 'html')
	            ->initContext();
    }
    
    //@ centerlised controller to index all the job created by the user 
    public function indexAction() {
        
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
			
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
        try {
            
            //@ handle the main get list all jobs created by me, sorted by recent on top
            if( $request->isGET() ) {

			$data_vendor = $db->fetchAll("select * from vendor where user_id = ?", array($UserSession->userId), 2);

            $data = $db->fetchAll("select * from job where category = ? order by added_on", array($data_vendor[0]['category']), 2);
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data );
                }
                
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                echo "In post request"; die;
                
            }
            
            
        } catch ( Exception $e ) {
            
        }
        
    }

    //@ centerlised controller to create new jobs 
    public function newAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
        
        try {
            
            // @ handle the main post request of the controller method
            if( $request->isGET() ) {
                $data = $db->fetchAll("select * from vendor_category order by id DESC", array(), 2);
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data );
                }                
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                //@ calculate the post variable
                $serviceCategory = $this->getRequest()->getPost('serviceCategory', null);
                $jobTitle   = $this->getRequest()->getPost('jobTitle', null);
                $startDate  = $this->getRequest()->getPost('startDate', null);
                $endDate    = $this->getRequest()->getPost('endDate', null);
                //$rating     = $this->getRequest()->getPost('rating', null);
                $myBudget    = $this->getRequest()->getPost('myBudget', null);
                $jobDescription = $this->getRequest()->getPost('jobDescription', null);
                
                if ( !is_numeric( $myBudget ) || $myBudget < 0 ) {
                    print(" Budget format invalid !!! "); exit;
                }
                
                // check if all fields have set
                if( $serviceCategory && $jobTitle && $startDate && $endDate && $myBudget && $jobDescription ) {
                    
                    // check job post date difference
                    list($y1,$d1,$m1) = explode('-', $startDate);
                    $date1 = date("d/m/y", mktime(0, 0, 0, $d1, $m1, $y1));
                    
                    
                    // check job post date difference
                    list($y2,$d2,$m2)=explode('-', $endDate);
                    $date2 = date("d/m/y", mktime(0, 0, 0, $d2, $m2, $y2));
                    
                    // substract two date values to get absolute value of the difference
                    $diff = abs(strtotime($date2) - strtotime($date1));
                    
                    // @ check if end date is greater than start date
                    if( strtotime($date2) < strtotime($date1) ) {
                        print(" Job's end date is less than job start date, kindly choose end date greater than start date !!!!"); exit;  
                    }
                    
                    $data = array(
                              'title'=>$jobTitle,
                              'description'=>$jobDescription,
                              'category'=>$serviceCategory,
                              'user_id'=>$UserSession->userId,
                              'budget'=>$myBudget,
                              'start_on'=>$startDate,
                              'end_on'=>$endDate,
                              'added_on'=>date("Y-m-d H:i:s"),
                              'updated_on'=>date("Y-m-d H:i:s")                              
                              );
                }
                
                if ( !$data ) {
                    // all values are empty (where "empty" means == false)
                    print(" All fields are required !!!,  "); exit;
                }
		
		if( $jobId = $this->getRequest()->getPost('jobId', null) ) {
		    //print($jobId); 
		    
		    //print_r($data); die;
		    if ( $db->update('job', $data, 'id='.$jobId.'') ){
			print (" Job Updated successfully !!!!"); exit;
		    }
		}
                
                if( $db->insert( "job", $data ) ) { // inserting data into to job table and check if it is a success
                    print(" Job created successfully !!!!"); exit;    
                } else {
                    print (" Unable to save the job please try again !!!!"); exit;
                }
            }
            
        } catch ( Exception $e ) {
            
            print( $e ); exit;
        }
        
    }
    
    //@ centerlised controller to edit existing jobs 
    public function editAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
	
	// begin the try catch block
	try {
	    
	    $jobId = $params['id'];
	    $owner = $params['owner'];
	    $data = $db->fetchAll("select * from job where id=? and user_id=?", array($jobId,$owner), 2);
	    if( $data ) {
		$this->view->data = array('data'=>$data);
	    } else {
		print "No Job selected !! or the job you are looking for not found !!"; exit;
	    }
	    
	} catch( Exception $e ) {
	    print $e;
	}
    }
    
    //@ centerlised controller to delete jobs 
    public function delAction() {   
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	try {	    
	    if( $request->isGET() ) {
		$id = $request->get('data', null );		
		$deleteJob = $db->delete('job', 'id = '.$id.'');
		if( $deleteJob ) {
		    $deleteApplier = $db->delete('appliers', 'job_id = '.$id.''); // working here delete job
		} else {
		    print("Failure, can't delete this job !"); exit;   
		}
		$this->view->data = array('data'=>$deleteApplier, 'job_appliers'=>$id, 'status'=>$deleteJob); // @ working here to do some basic operations on applier.
	    }
	    
	} catch( Exception $e ) {
	    print $e;
	}
    }
    
    // @ actions on the applier
    public function operationonapplierAction() { 
	$db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
        
        // create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	try {
	    
	    if( $request->isGET() ) {
		
		//$operation = $request->getPost('data', null ); sendmessage_9_1
		list($type, $job, $applier) = explode("_", $request->get('data', null ));
		$data = $db->fetchAll("select * from vendor as v, user as u where v.user_id=? and v.user_id=u.id", array($applier), 2);
		$messages = $db->fetchAll("select m.id,m.conv_id, m.sent_to,m.by_from,m.message,m.date,u.first_name, u.last_name,u.email from messages as m left join conversation as c on m.conv_id = c.id left join user as u on m.by_from = u.id  where c.job_thread = ? and m.status in (0,1)", 'job_'.$job , 2);

		 if(count($messages) > 0 ){
			$jobMess = $messages;
		    }else{
			$jobMess = NULL;
		    } 

		if( $data ) {
		    $this->view->data = array('data'=>$data, 'type'=>$type, 'job'=> $job, 'messages' => $jobMess); // @ working here to do some basic operations on applier.
		} else {
		    print("Vendor not found !!"); exit;   
		}		
	    }
	    
	    if( $request->isPost() ) {
		 
		$conversation = $this->getRequest()->getPost('conversationOn');
		$applierMessage = $this->getRequest()->getPost('applierMessage');
		$withWho = $this->getRequest()->getPost('withWho');
		$job_thread = $this->getRequest()->getPost('job_thread');
		$byWho = $UserSession->userId;
		
		if( $conversation &&  $applierMessage && $withWho && $byWho ) { // check if all fields are set
		    $convo =  $this->getRequest()->getPost('conv_id');  //$db->fetchAll("select * from conversation where name=?", array($conversation), 2);
		    if( $convo ) { // check if conversation already exist
			$data = array(
				  'conv_id'=>$convo,
				  'sent_to'=>$withWho,
				  'by_from'=>$byWho,
				  'message'=>$applierMessage,
				  'date' => date("Y-m-d H:i:s")	
				  );
			if ( $db->insert("messages", $data) ) {
			    print ("Success, your message has been sent"); exit; 
			} else {
			    print ("Error, Try again"); exit;
			};
		    } else { // If it is new thread insert new conversation and fire message to user
			$data = array(
				  'name'=>$conversation,
				  'with_who'=>$withWho,
				  'by_who'=>$byWho,
				  'job_thread'=> $job_thread,
				  'date'=>date("Y-m-d H:i:s"),
				  );
			if( $db->insert("conversation", $data) ) {
			    
			    $lastInsertedId = $db->lastInsertId();
			    $data = array(
				  'conv_id'=>$lastInsertedId,
				  'sent_to'=>$withWho,
				  'by_from'=>$byWho,
				  'message'=>$applierMessage,
				  'date' => date("Y-m-d H:i:s")
				  );
			    if ( $db->insert("messages", $data) ) {
				print ("Success, your message has been sent"); exit; 
			    } else {
				print ("Error, Try again"); exit;
			    };
			}
		    }
		} else {
		    print("All fields are required !!"); exit; 
		}
	    }
	    
	} catch ( Exception $e ) {
	    print $e;
	}
    }
    
    //@ centerlised controller to edit existing jobs 
    public function jobapplicationAction() {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// create zend object to handle the main http request
        $request = new Zend_Controller_Request_Http;
	
	// get the list of all parameters in query string
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
	
	// try catch begins
	try {
	    // to display send job application form in sidebar
	    if( $request->isGET() ) {
		
		$jobId = $params['data'];  
		
		// check if this user has already applied for the job
		$alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=?", array($jobId, $UserSession->userId), 2);
		if ( $alreadyApplied ) {
		    
		    print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
		}
		
		// Apply for this job as below
		$job_applier = $db->fetchAll("select v.user_id, jo.id, jo.title ,jo.start_on , jo.end_on from vendor as v, job as jo where v.user_id=? and jo.id=?", array($UserSession->userId, $jobId), 2);
		if( $job_applier ) {
		    $this->view->data = array('data'=>$job_applier);
		} else {
		    print ("Error, can apply for this job. You can notify admin about this  generating support ticket !!, or try again"); 
		}
	    }
	    
	    // to display send job application form in sidebar
	    if( $request->isPOST() ) {
		
		// @ get the all post variables
		$quoteContent = $this->getRequest()->getPost('quoteContent');
		$jobId = $this->getRequest()->getPost('jobId');
		$applierId = $this->getRequest()->getPost('applierId');


		$alreadyApplied = $db->fetchAll("select * from appliers where job_id=? and applier_id=?", array($jobId, $UserSession->userId), 2);
		if ( $alreadyApplied ) {
		    
		    print("You have already applied for this JOB, you can edit or reply from your dashboard if you want to !!"); exit;
		}
    
		// @ check if all parameters are valid
		if( $quoteContent && $jobId && $applierId ) {
		    
		    $data = array(
				  'vendor_id'=>$applierId,
				  'job_id'=>$jobId,
				  'quote_content'=>$quoteContent,
				  'added_on'=>date("Y-m-d H:i:s"),
				  'updated_on'=>date("Y-m-d H:i:s")
				  );
		    $n = $db->insert("quotes", $data);
		    if( $n ) {
			$data = array(
				      'applier_id'=>$applierId,
				      'job_id'=>$jobId,
				      'added_on'=>date("Y-m-d H:i:s"),
				      'updated_on'=>date("Y-m-d H:i:s")				      
				      );
			$application = $db->insert("appliers", $data);
			if( $application ) {
			    
			    print("Application has been sent, successfully"); exit;
			    
			}
		    }
		    
		} else {
		    print("kindly fill the required fields"); exit;
		}
		
	    }
	    
	} catch ( Exception $e ) {
	    print $e;
	}
	
    }
}
    
?>
