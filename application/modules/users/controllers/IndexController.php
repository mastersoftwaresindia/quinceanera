<?php

class Users_IndexController extends Zend_Controller_Action {

    protected $_redirector = null;
    public function init() {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('signup', 'html')
                    ->addActionContext('signin', 'html')
                    ->addActionContext('referer-account', array('html','json'))
		    ->addActionContext('gmail-contacts', 'html')
		    ->addActionContext('fymsn-contacts', 'html')
	            ->addActionContext('password-activation', 'html')
                    ->initContext();
    }
    public function indexAction() {
      
        // print "Hello"; die;
	$UserSession = Zend_Registry::get('UserSession');
	if( $UserSession->userId ) {
	    
	    $urlOptions = array('module'=>'users', 'controller'=>'dashboard', 'action'=>'index');
	    $this->_helper->redirector->gotoRoute($urlOptions);
	    
	} 
    	
    }
    public function signupAction() {
        
        $db=Zend_Registry::get("db");        
        if($this->getRequest()->isPost()) {
	    
	    if ( $db->getConnection() ) {

                $email = $_POST['email']; 
                $password =$_POST['password']; 
                $activation_key = (rand(1000,10000));                
                $sql = 'SELECT * FROM user WHERE email = ?'; 
                $result = $db->fetchAll($sql, $email);
                if(count($result) == 0) {
		    $data = array(
		      'username'       => '',
		      'email'          => $email,
		      'password'       => md5($password),
		      'activation_key' => $activation_key,
		      'status'         => '0'
		    );  
		    $db->insert('user', $data);
		    $id = $db->lastInsertId('user');
		    $UserSession = Zend_Registry::get('UserSession');
		    //$UserSession->userId = $id;
		     
		    // send token to user using zend email method.                   
		    $this->sendMailToUser($activation_key, $email);
							
		    $data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Your registration is complete, kindly check your email for account activation code.', 'resp'=> 'success');
		    $this->view->data = $data;
                } else { 
                    $data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Email id already exists.' , 'resp'=> 'error');
                    $this->view->data = $data;
                }
            } else {
		    echo "Not connected".mysql_error();
	    }
        }
        // Get Request
        if($this->getRequest()->isGET()) {
            $data = array('method'=>$this->getRequest()->getMethod());
            $this->view->data = $data;
        }
    }


    public function signinAction() {
      
        $db=Zend_Registry::get("db");

        if($this->getRequest()->isPost()) {
            
            if ( $db->getConnection() ) {
                    $UserSession = Zend_Registry::get('UserSession');
                    $email = $_POST['email']; 
                    $UserSession->userEmail = $email;     //set session mail
                    $password =$_POST['password'];   
                    $sql = 'SELECT * FROM user WHERE email = ?';     
                    $result = $db->fetchAll($sql, $email);
		    if($result) {
			$role = $result[0]->role;
			if(count($result) > 0 && md5($password) == $result[0]->password) {
			    if($result[0]->status=='1'){
				if($role == 0){
			    		$UserSession = Zend_Registry::get('UserSession');
			    	}elseif($role == 2){
			    		$UserSession = Zend_Registry::get('VendorSession');
			    	}                      
                $UserSession->userEmail = $email;        	
                $UserSession->userId = $result[0]->id;
				$UserSession->userStatus = $result[0]->status;
				$UserSession->userRole = $result[0]->role;
				$updat = array('on_off'=> 1);
				$updateUser = $db->update('user', $updat, 'id ='.$UserSession->userId);
                        	$data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Success, redirecting please wait....','status'=>'valid','role'=>$role);
                       		$this->view->data = $data;                                            
			    }else{
				$data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'You have not activated your account.','status'=>'invalid','role'=>'');
                       		$this->view->data = $data;
			    }
			} else {
				$data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Please check your username and password.','status'=>'invalid','role'=>'');
				$this->view->data = $data;                       
			}
		    }
                    
            } else {
                echo "Not connected".mysql_error();
            }
        }

        // Get Request
        if($this->getRequest()->isGET()) {
            $data = array('method'=>$this->getRequest()->getMethod());
            $this->view->data = $data;          
        }
    }

    public function accountActivationAction() {
        try {          
            $db=Zend_Registry::get("db");	    
            if ( $db->getConnection() ) {

                if($this->getRequest()->isPost()) {
                    // check user status before activation.
		     $activation_code = $_POST['activation_code']; 
		    $query = 'SELECT * FROM user WHERE id = ?';
		    $resp = $db->fetchAll($query, $UserSession->userId);
		    $this->view->userStatus = $resp[0]->status ;
                   
                    $sql = 'SELECT * FROM user WHERE activation_key = ?';
     
                    $result = $db->fetchAll($sql, $activation_code);
    
                   
                    if (trim($result[0]->activation_key) == trim($activation_code)){

			$data = array(
                                  'activation_key'      => '',
                                  'status'              => 1
                               );
		
			// echo $UserSession->userId;die;
                       
			$updateUser = $db->update('user', $data, 'id ='.$result[0]->id);
			if($updateUser){
		       	    // $this->_redirector->gotoSimple('index', 'user-step', null);
			    $this->redirect(PUBLIC_URI.'/users/index/user-step');
			}else{
			    $this->view->message = "your activation code has been matched !!";
			}
                       
                    }else{
                    
                       $this->view->message = "Oops, you have entered wrong or expired activation code.";
                    
                    }   
                }

            } else {
                echo "Not connected".mysql_error();
            }   
        
        } catch (Zend_Db_Adapter_Exception $e) {
		// perhaps a failed login credential, or perhaps the RDBMS is not running
		print $e;
        
        } catch (Zend_Exception $e) {
		// perhaps factory() failed to load the specified Adapter class
		print $e;
        }    
    }


    public function sendMailToUser($activation_key, $email) {

	 	// create view object

		Zend_Loader::loadClass('Zend_View');
		$html = new Zend_View();
		$html->setScriptPath(APPLICATION_PATH . '/modules/users/views/scripts/email/');
		
		// assign values

		$html->assign('activation_key', $activation_key);
		$html->assign('email', $email);
				
		// create mail object

		$mail = new Zend_Mail('utf-8');

		// render view
		$bodyText = $html->render('accountActivationemail.phtml');

		// configure base stuff
		$mail->addTo($email, '');
		$mail->setSubject('Quinceanera Account Activation Mail');
		$mail->setFrom('admin@quinceara.com', 'Email From Quinceanera.');
		$mail->setBodyHtml($bodyText);		      
		$mail->send();
       
    }

    public function sendPasswordResetMail($password_activation_key, $email) {

        // create view object
        Zend_Loader::loadClass('Zend_View');
        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/modules/users/views/scripts/email/');
      
        // assign values
        $html->assign('password_activation_key', $password_activation_key);
        $html->assign('email', $email);
        
        // create mail object
        $mail = new Zend_Mail('utf-8');
        $bodyText = $html->render('passwordResetemail.phtml');
        $mail->addTo($email, '');
        $mail->setSubject('Quinceanera Password Reset');
        $mail->setFrom('admin@quinceara.com', 'Email from quinceanera.');
        $mail->setBodyHtml($bodyText);

        $mail->send();
       
    }

    public function passwordActivationAction() {
    
	$db=Zend_Registry::get("db");

	if($this->getRequest()->isPost()) {

            $email = $_POST['email']; 
	    // echo $email; die;
            $sql = 'SELECT * FROM user WHERE email = ?';
     
            $result = $db->fetchAll($sql, $email);
            //echo "<pre>"; print_r($result); die;   
                
            if(count($result) > 0) {
		$UserSession = new Zend_Session_Namespace('UserSession');
		$UserSession->userId = $result[0]->id;
		 // echo $UserSession->userId;die;
	       
		$password_activation_key = (rand(10000,20000)); 
		  //echo $password_activation_key; die;
        
                $data = array(
                               'password_activation_key'  => $password_activation_key,
                             );
                $updateUser = $db->update('user', $data, 'id ='.$UserSession->userId);
                $this->sendPasswordResetMail($password_activation_key, $email);
		       $this->_helper->json(
              			array('message' => 'You have successfully requested for Password Reset request.Please check your Email to get Password Activation Code.redirecting, please wait...' ,
                    			'resp' => 'success'
              			)
            		);
            	return;
            } else {
		$this->view->message = "This Email Id is not registered.";
            }
    
        }
    }
 
    public function forgetPasswordAction() {
      
	$db = Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	$id = $UserSession->userId;                                  //read session variable
      
	if($this->getRequest()->isPost()) {
        
	    $password_reset_code = $_POST['password_reset_code']; 
	    $new_password = $_POST['new_password']; 
    
	    $sql = 'SELECT * FROM user WHERE id = ?';
	 
	    $result = $db->fetchAll($sql, $id);    
	    $db_password_key = $result[0]->password_activation_key;	    
    
	    if($password_reset_code == $db_password_key){
		
		$UserSession = new Zend_Session_Namespace('UserSession');
		
		    $data = array(
			    'password' => md5($new_password),
			    'password_activation_key'  => ''
			);
		    // echo $UserSession->userId;die;
		    $updateUser = $db->update('user', $data, 'id ='.$id);
		       
		    $data = array('data'=> 'Password Reset successfully.','status'=>'valid');
		    $this->view->data  = $data;
	    } else {    
		    $data = array('data'=> "Password Reset Code doesn't match" ,'status'=>'invalid');
		    $this->view->data  = $data;       
	    }
	}
    }

    public function logoutAction() {
	$db=Zend_Registry::get("db");
	$request = new Zend_Controller_Request_Http;
        if($request->get('role') == 0){
    		$UserSession = new Zend_Session_Namespace('UserSession');
    	}elseif($request->get('role') == 2){
    		$UserSession = new Zend_Session_Namespace('VendorSession');
    	}
	$updat = array('on_off' => 0);
	$updateUser = $db->update('user', $updat, 'id ='.$UserSession->userId);
        unset($UserSession->userId);
	unset($UserSession->userStatus);
	unset($UserSession->userRole);
	$this->getResponse()->setRedirect(PUBLIC_URI);
    }

    public function userProfileAction() {
	$db=Zend_Registry::get("db");
    	
     	if($this->getRequest()->isGET()) {
		$UserSession = new Zend_Session_Namespace('UserSession');
		if(isset($UserSession->userId)){		
  		    $sql = 'SELECT * FROM user WHERE id = ?';
                    $result = $db->fetchAll($sql, $UserSession->userId);
		    $this->view->data = array('status' => $result[0]->status);
		}
	}
    }

    public function userStepAction() {
	$db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();

	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){
 			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			if( isset($params['id'] )) {		
			    $editEvents = $db->fetchAll("select * from bookings where id=?", array( $params['id'] ), 2);
		         
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media, 'editInfo' => $editEvents);
		    

			}else{
	    		  
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);

			}
		}		
	}
	if($this->getRequest()->isPost()) {
	    if ( $db->getConnection() ) {
		if(isset($_POST['contactss'])){
			$contsc = implode( ',', $_POST['contactss'] );		

		}else{
			$contsc = '';
		}

		$userGroup          = (isset($_POST['user_group'])) ? $_POST['user_group'] : '';
		$userdate           = (isset($_POST['usetdate'])) ? $_POST['usetdate'] : '';
		$religion           = (isset($_POST['religion'])) ? $_POST['religion'] : '';
		$ceremony           = (isset($_POST['ceremony'])) ? $_POST['ceremony'] : '';
		$damas              = (isset($_POST['damas'])) ? $_POST['damas'] : '';
		$chambelanes        = (isset($_POST['chambelanes'])) ? $_POST['chambelanes'] : '';
		$weddingDress       = "#".$_POST['dresscolor1'].",#".$_POST['dresscolor2'];
		$weddingTheme       = "#".$_POST['themecolor1'].",#".$_POST['themecolor2']; 
		$quinceType         = (isset($_POST['quince'])) ? $_POST['quince'] : '';
		$guests             = (isset($_POST['guests'])) ? $_POST['guests'] : '';
		$spentmoney         = (isset($_POST['spentmoney'])) ? $_POST['spentmoney'] : '';
		$moneysofor         = (isset($_POST['moneysofor'])) ? $_POST['moneysofor'] : '';
		$createPlan         = (isset($_POST['create_plan'])) ? $_POST['create_plan'] : '';
		$imported_contacts  = $contsc;
		$anotherUserAccount = (isset($_POST['anotheruser'])) ? $_POST['anotheruser'] : '';
    
		$optionaData = array(	
		  'user_id'              => $UserSession->userId,
		  'user_group'           => $userGroup,
		  'user_date'            => $userdate,
		  'religion'             => $religion,
		  'ceremony'             => $ceremony,
		  'damas'                => $damas,
		  'chambelanes'          => $chambelanes,
		  'wedding_dress'        => $weddingDress,
		  'wedding_theme'        => $weddingTheme,
		  'quince_type'          => $quinceType,
		  'guests'               => $guests,
		  'event_money'          => $spentmoney,
		  'saved_so_for'         => $moneysofor,
		  'create_plan'          => $createPlan,
		  'imported_contacts_type' => $imported_contacts,
		  'another_user_account' => $anotherUserAccount
		);
		
		// $data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Optional data has been saved successfully.', 'resp'=> 'success');
		if( isset($params['id'] )) {
			$val = $db->update('bookings', $optionaData, 'id ='.$params['id']);	
		}else{
			$val = $db->insert( 'bookings', $optionaData );
		}
		if ( $val  ) {
			$this->view->message = "We have received your information successfully.";
		} else {	      
			$this->view->message = "We have not received any information about your event, kindly fill out all the required fields in user steps.";
		}
	    } else {
		echo "Not connected".mysql_error();
	    } 
	}
    } 

    public function refererAccountAction() {
	    $db=Zend_Registry::get("db");
      
	    if ( $db->getConnection() ) {
		$otherUser = $_POST['otheruser'];
		$otherpassword = $_POST['otherpassword'];  
		$activation_key = (rand(1000,10000)); 
		$UserSession = new Zend_Session_Namespace('UserSession');
	
		$sql = 'SELECT * FROM user WHERE email = ?';
		$result = $db->fetchAll($sql, $otherUser);	
		if(count($result) == 0) {
		    $otherUserData = array(
		      'username'       => '',
		      'email'          => $otherUser,
		      'password'       => md5($otherpassword),
		      'role'	       => 5,
		      'referer'        => $UserSession->userId,
		      'activation_key' => $activation_key,
		      'status'         => '0'
		    );
	  
		    $n = $db->insert('user', $otherUserData);
		    // send token to user using zend email method. need to uncomment when code is on server.
		    if( $n ) {
		    $this->sendMailToUser($activation_key, $otherUser);
      
		    $this->_helper->json(
			array('message' => 'Added another user successfully for this account.' ,
			      'resp' => 'success'
			)
		    );
		    return;
		   }
		} else { 
		    $this->_helper->json(
			array('message' => 'Email id already exists.' ,
			      'resp' => 'error'
			)
		    );
		    return;
		}
      
	    } else {
		    echo "Not connected".mysql_error();
	    }
    }

    /* Import Gmail contacts */
    public function gmailContactsAction() {
	if($this->getRequest()->isGET()) { 
	    $client_id     = '830398842315-pq4jo2monq8bo4vi1ium6ajd48h97eac.apps.googleusercontent.com';
	    $client_secret = 'CkrhxNQV2m1RypFa4EIw9bUO';
	    $redirect_uri  = 'http://mastersoftwaretechnologies.com/za/public/users/index/gmail-contacts';
	    $max_results   =  100000;  
	    $auth_code = $_GET["code"];
	    
	    $fields=array(
	      'code'=>  urlencode($auth_code),
	      'client_id'=>  urlencode($client_id),
	      'client_secret'=>  urlencode($client_secret),
	      'redirect_uri'=>  urlencode($redirect_uri),
	      'grant_type'=>  urlencode('authorization_code')
	    );
	    $post = '';
	    foreach($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }
	    $post = rtrim($post,'&');
	     
	    $curl = curl_init();
	    curl_setopt($curl,CURLOPT_URL,'https://accounts.google.com/o/oauth2/token');
	    curl_setopt($curl,CURLOPT_POST,5);
	    curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,FALSE);
	    $result = curl_exec($curl);
	    curl_close($curl);
	     
	    $response =  json_decode($result);
	    $accesstoken = $response->access_token;
	     
	    $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results='.$max_results.'&oauth_token='.$accesstoken;
	    $xmlresponse =  $this->curl_file_get_contents($url);
	    if((strlen(stristr($xmlresponse,'Authorization required'))>0) && (strlen(stristr($xmlresponse,'Error '))>0)) 
	    {
	      echo "<h2>OOPS !! Something went wrong. Please try reloading the page.</h2>";
	      exit();
	    }
	    $xml =  new SimpleXMLElement($xmlresponse);
	    $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
	    $result = $xml->xpath('//gd:email');
		     
	    $this->view->data = $result;

	}     
        if($this->getRequest()->isPost()) {
	  $socialType = 'Gmail';
	  $this->saveContacts($_POST['contacts'], $socialType);
	} 
    }

    public  function curl_file_get_contents($url) {
        $curl = curl_init();
        $userAgent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0)';
       
        curl_setopt($curl,CURLOPT_URL,$url); //The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);  //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5); //The number of seconds to wait while trying to connect.  
       
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);  //To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($curl, CURLOPT_TIMEOUT, 10); //The maximum number of seconds to allow cURL functions to execute.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); //To stop cURL from verifying the peer's certificate.
       
        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
    }

    /* Import Yahoo, Facebook Contacts*/
    public function fymsnContactsAction() {   
      // Set headers for CORS DOMAIN
      header('Access-Control-Allow-Origin: *');
      header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
      header('Access-Control-Allow-Headers: Content-Type');
      header('Access-Control-Allow-Credentials: true');   
      // check request type
      if($this->getRequest()->isPost()) {
        $db = Zend_Registry::get("db");
        if ( $db->getConnection() ) {
          if(isset($_POST['ses'])){
            foreach ($_POST['fymsncontacts'] as $key => $value) {
              $data = array(
                  'referer'=> $_POST['ses'],
                  'email'  => $value,
                  'type'   => $_POST['service']
              );
              $db->insert('contacts', $data);
            }
          
          $this->_helper->json(
            array('message' => 'Your Contacts has been saved Successfully !! ' ,
                  'resp' => 'success'
            )
          );
          return;
        }else{
            // show message please login.
           $this->_helper->json(
            array('message' => 'Oops, Contacts not saved !!' ,
                  'resp' => 'fail'
            )
          );
          return;
          }
        } else {
          echo "Not connected".mysql_error();
        }
      }       
      
    }

    /* MSN contacts import here. */
    public function msnContactsAction() {
      if($this->getRequest()->isGET()) {
        $client_id = '0000000048120AFE';
        $client_secret = 'TWkKv7Nj5PCYEx7dJtR5Yw6PTcM1p-Zm';
        $redirect_uri = 'http://mastersoftwaretechnologies.com/za/public/users/index/msn-contacts';

        $auth_code = $_GET["code"];
        $fields = array(
        'code'          =>  urlencode($auth_code),
        'client_id'     =>  urlencode($client_id),
        'client_secret' =>  urlencode($client_secret),
        'redirect_uri'  =>  urlencode($redirect_uri),
        'grant_type'    =>  urlencode('authorization_code')
        );
        $post = '';
        foreach($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }

        $post = rtrim($post,'&');
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,'https://login.live.com/oauth20_token.srf');
        curl_setopt($curl,CURLOPT_POST,5);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
        $result = curl_exec($curl);
        curl_close($curl);
        $response =  json_decode($result);
        $accesstoken = $response->access_token;
        $url = 'https://apis.live.net/v5.0/me/contacts?access_token='.$accesstoken.'&limit=1000000';
        $xmlresponse =  $this->curl_file_get_contents($url);
        $xml = json_decode($xmlresponse, true);
        $this->view->data = $xml['data'];
 
      }
      // Save contacts into database.
      if($this->getRequest()->isPost()) {
      	$socialType = 'msn';
        $this->saveContacts($_POST['contacts'], $socialType);
      } 


    }

    public function saveContacts($contacts, $type){
        $db=Zend_Registry::get("db");
        if ( $db->getConnection() ) {
          $UserSession = new Zend_Session_Namespace('UserSession');
          if(isset($UserSession->userId)){
            foreach ($contacts as $key => $value) {
              $data = array(
                  'referer'=> $UserSession->userId,
                  'email'  => $value,
                  'type'   => $type
              );
              $db->insert('contacts', $data);
            }
          
            $this->_helper->json(
              array('message' => 'Your Contacts has been saved Successfully !! ' ,
                    'resp' => 'success'
              )
            );
            return;
          }else{
            // show message please login.
             $this->_helper->json(
              array('message' => 'Oops, Contacts not saved !!' ,
                    'resp' => 'fail'
              )
            );
            return;
          }
        } else {
          echo "Not connected".mysql_error();
        }  

    }

     public function facebookLoginAction() {

	$db=Zend_Registry::get("db");
	$UserSession = Zend_Registry::get('UserSession');
	require './Facebook/facebook.php';
	include('./Facebook/config.php');	
	$facebook = new Facebook(array(
		'appId'  => $APP_ID,
		'secret' => $APP_SECRET
	));
	$user = $facebook->getUser();

	if($user != 0){
	     $api = $facebook->api('me');

	    $fbId = $api['id'];
	    $fbfname = $api['first_name'];
	    $fblname =  $api['last_name'];
	    $fbgender = $api['gender'];
	    $fbname = $api['name'];
	     
	    $sql = 'SELECT * FROM user WHERE fb_id =?';     
            $result = $db->fetchAll( $sql, $fbId );
	    if(count($result) > 0 ) {
		if(!empty($result[0]->email)){
		 	$fbres = $result[0]->email;
		
		}else{
			$fbres = $result[0]->name;
		}
		$UserSession->userId = $result[0]->id;
		$UserSession->userEmail = $fbres;
		$UserSession->userStatus = $result[0]->status;
		$UserSession->userRole = $result[0]->role;
		$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
                $this->_helper->redirector->gotoRoute($urlOptions);
	    }else {
		if(isset($api['email'])){
		 	$fbem = $api['email'];
		
		}else{
			$fbem = '';
		}
		$data = array(
		    'email'          => $fbem,
		    'role'	     => '0',
		    'name'           => $fbname,
		    'first_name'     => $fbfname,
		    'last_name'      => $fblname,
		    'gender'	     => $fbgender,
		    'status'         => '1',
		    'fb_id'          => $fbId
		);  

		$db->insert('user', $data);
		if(isset($api['email'])){
			$fbemail = $api['email'];
		}else{
		        $fbemail = $fbname;	
		}
		$id = $db->lastInsertId('user');
		$UserSession->userId = $id;
		$UserSession->userEmail = $fbemail;
		$UserSession->userStatus = 1;
		$UserSession->userRole = 0;
		$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
                $this->_helper->redirector->gotoRoute($urlOptions);
	    }
	} else {
	   die('not connected to facebook');
	
	}
    }

	 public function googleLoginAction(){
	        $db=Zend_Registry::get("db");
	        $UserSession = new Zend_Session_Namespace('UserSession');

    		require_once 'Google/src/Google_Client.php';
		require_once 'Google/src/contrib/Google_Oauth2Service.php';
		require_once 'Google/config.php';

		$gClient = new Google_Client();
		$gClient->setClientId($GOOGLE_CLIENT_ID);
		$gClient->setClientSecret($GOOGLE_CLIENT_SECRET);
		$gClient->setRedirectUri($GOOGLE_REDIRECT_URL);
		$gClient->setDeveloperKey($GOOGLE_DEVELOPER_KEY);

		$google_oauthV2 = new Google_Oauth2Service($gClient);
		if (isset($_REQUEST['reset']))
		{
			$gClient->revokeToken();
		}

		if (isset($_GET['code'])) 
		{ 
			 $gClient->authenticate($_GET['code']);
			 $token = $gClient->getAccessToken();
		}
		
		if (isset($token)) 
		{ 
			$gClient->setAccessToken($token);
		}

		if ($gClient->getAccessToken()) 
			
		{

			  $user = $google_oauthV2->userinfo->get();
			  $gmail = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			  $data = array(
				  'google_id' 	=> $user['id'],
				  'name'        => $user['name'],
				  'email' 	=> $gmail,
			          'status'	=> '1',
				  'first_name' 	=> $user['given_name'],
				  'last_name' 	=> $user['family_name'],
				  'gender' 	=> $user['gender']
		       );
			    $sql = 'SELECT * FROM user WHERE google_id = ?';     
			    $result = $db->fetchAll($sql,$user['id']);
			    if(count($result) > 0 ) {
				$UserSession->userId = $result[0]->id;
				$UserSession->userEmail = $result[0]->email;
				$UserSession->userStatus = $result[0]->status;
				$UserSession->userRole = $result[0]->role;
				$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
				$this->_helper->redirector->gotoRoute($urlOptions);
			    }else {
			    	$db->insert('user', $data);
				$id = $db->lastInsertId('user');
				$UserSession->userId = $id;
				$UserSession->userEmail = $gmail;
				$UserSession->userStatus = 1;
				$UserSession->userRole = 0;
				$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
				$this->_helper->redirector->gotoRoute($urlOptions);
			   }
    		}
	}
	
	public function twitterLoginAction(){
		$db=Zend_Registry::get("db");
	    $UserSession = new Zend_Session_Namespace('UserSession');

	    require_once('./Twitter/twitteroauth/twitteroauth.php');
		include('./Twitter/config.php');

		$connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET);
		$request_token = $connection->getRequestToken($OAUTH_CALLBACK); //get Request Token
		
		if(	$request_token )
		{ 
			$token = $request_token['oauth_token'];
			$UserSession->request_token = $token ;
			$UserSession->request_token_secret = $request_token['oauth_token_secret'];

			switch ($connection->http_code) 
			{
				case 200:

				    $url = $connection->getAuthorizeURL($token);
				    //die ($url."codeinside");
				    $this->_redirect($url);
				    break;
				default:
				    echo "Conection with twitter Failed";
			    	break;
			}
		}
		else //error receiving request token
		{
			die( "Error Receiving Request Token" );
		}		
	}

	public function twitterOauthAction(){
		require_once('./Twitter/twitteroauth/twitteroauth.php');
		include('./Twitter/config.php');
		$db=Zend_Registry::get("db");
		$UserSession = new Zend_Session_Namespace('UserSession');
		if(isset($_GET['oauth_token']))
		{

			$connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $UserSession->request_token, $UserSession->request_token_secret);
			$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
			if($access_token)
			{
				$connection = new TwitterOAuth($CONSUMER_KEY, $CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
				$params =array();
				$params['include_entities']='false';
				$content = $connection->get('account/verify_credentials',$params);
				//print_r($content);die;
				if($content && isset($content->screen_name) && isset($content->name))
				{
					$twitterid = $content->id;
					$name = $content->name;
					$data = array(
						'name'	        =>	$name,
						'twitter_id'	=>	$twitterid,
						'username'	=>	$content->screen_name,
						'status'	=>      1
				     );
				     unset($UserSession->request_token);
				     unset($UserSession->request_token_secret);
				     $sql = 'SELECT * FROM user WHERE twitter_id = ?';     
				     $result = $db->fetchAll($sql,$twitterid);
				     if(count($result) > 0 ) {
		    			if(!empty($result[0]->email)){
						 	$twitter_email = $result[0]->email;
						
						}else{
							$twitter_email = $result[0]->name;
						}
						$UserSession->userId = $result[0]->id;
						$UserSession->userEmail = $twitter_email;
						$UserSession->userStatus = $result[0]->status;
						$UserSession->userRole = $result[0]->role;
						$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
						$this->_helper->redirector->gotoRoute($urlOptions);
				    }else {

				    	$db->insert('user', $data);
						$id = $db->lastInsertId('user');
						$UserSession->userId = $id;
						$UserSession->userEmail = $name;
						$UserSession->userStatus = 1;
						$UserSession->userRole = 0;
						$urlOptions = array('module'=>'default', 'controller'=>'', 'action'=>'');
						$this->_helper->redirector->gotoRoute($urlOptions);
					}
				}

		else
		{

			echo "<h4> Login Error </h4>";
		}

		}

	}
}
}



    
