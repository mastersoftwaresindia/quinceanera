<?php

class Users_DashboardController extends Zend_Controller_Action
{
    protected $_redirector = null;
    public function init()
    {
        /* Initialize action controller here */
	$this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('budgetpaynow', 'html')
		    ->addActionContext('addnewservice', 'html')
		    ->addActionContext('favorite', 'html')
		    ->addActionContext('deleteevent', 'html')
		    ->addActionContext('guestlistoperation', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $resultMedia = $db->fetchAll("SELECT * FROM media", array(), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
        
    }
    
    public function mywebsiteAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    print_r($resultMedia);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
        
    }

    public function mypicAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    print_r($resultMedia);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
        
    }
    
    public function profileAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    //echo "session expire";
	}
	
	if($this->getRequest()->isPOST()) {
	    
		try {
		    $db=Zend_Registry::get("db");
		    // pretend this is a sophisticated database query
		    $data = array(
                                'first_name'      => $_POST['first_name'],
                                'last_name' => $_POST['last_name'],
				'date_of_birth' => $_POST['date_of_birth'],
				'gender' => $_POST['gender'],
				'age' => $_POST['age'],
				'bio' => $_POST['bio'],
				'address' => $_POST['address'],
				'pincode' => $_POST['pincode'],
				'phone' => (int)$_POST['phonenumber'],
				'email' => $_POST['email'],
				'fb_name' => $_POST['fb_name'],
				'tw_name' => $_POST['tw_name'],
				'sky_name' => $_POST['sky_name'],
				'gplus_name' => $_POST['gplus_name'],
				'website' => $_POST['website']
                            );		   
		    $n = $db->update('user', $data, 'id = '.$UserSession->userId.'');
		    $sql = 'SELECT * FROM user WHERE id = ?';
		    $result = $db->fetchAll($sql, $UserSession->userId);
		    $usersection = 'user-avatar';
		    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                    if($resultMedia){
			$media = $resultMedia ;
		    }else {
			$media = '';
		    }
		    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
	}
        
    }
    
    //@jeevan front end mailbox
    public function mailboxAction() {
	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
	if($this->getRequest()->isGET()) {
		   /* if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    $messages = $db->fetchAll("SELECT * FROM messages WHERE sent_to=? group by conv_id", array( $UserSession->userId ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media, 'messages'=>$messages);
		    }*/
	            try{   
                $result = $db->fetchAll("SELECT  a.* FROM    messages a INNER JOIN ( SELECT  conv_id, MAX(ID) max_ID FROM messages where sent_to = ? GROUP BY conv_id order by id desc) b ON  a.conv_id = b.conv_id AND a.ID = b.max_ID", array($UserSession->userId), 2);
	//print_r($result);die;
                $user = $db->fetchAll("select * from user", array(), 2);
		$ids = array();
                if(isset($result)){
                    foreach($result as $key=> $data) {
                        $ids[] = $data['by_from'];
                    }
                }
                $id = implode(",", $ids );
                
$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($id)", array(), 2);
                if( $result ) {                    
                    $this->view->data = array('messages'=>$result, 'user'=> $user, 'media'=>$media);                    
                } else {                    
                    $this->view->data = NULL;                    
                }            
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
	}
	
    }
    
    //@jeevan front end fetch all contacts of the logged in user
    public function contactsAction() {
	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM contacts WHERE referer = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
	}
	
    }
    
    //@jeevan front end fetch all vendors of the logged in user
    public function myvendorsAction() {
	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM contacts WHERE referer = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
	}
	
    }
	public function venderlistAction()
    {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
        if($this->getRequest()->isGET()) {
            $sql = 'SELECT * from user left join favorites on favorites.favorite_id=user.id  where user_id=?';
            $result = $db->fetchAll($sql, $UserSession->userId, 2); 
           //echo "<pre>";print_r($result); die;
            $userIds = [];
            foreach ($result as $key => $value) {
                array_push($userIds,$value['favorite_id']);
            }
            $ids = implode(',',$userIds);
           // echo($ids); die();
            if ($ids) {
                echo "Inside query";
                $images = $db->fetchAll("SELECT * FROM media where owner in ('$ids')", array(), 2);
            }
          // print_r($images); die;
            if($result){
                $users = $result ;
            }else {
                $users = NULL;
            }
	    $usersection = 'user-avatar';
		$resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                if($resultMedia){
		    $media = $resultMedia ;
		    }else {
		    $media = '';
		    }
            $this->view->data = array('users' => $users , 'media1' => $images,'media' => $media);
          //  echo "<pre>"; print_r($this->view->data); die;
    	} 
    }

    public function receiveAction()
    {
        $UserSession = new Zend_Session_Namespace('UserSession');
        $chatWith=$_POST['receiver'];
        $lastreceived=$_POST['lastreceived'];
        $filename="./chat/room_".$chatWith."_".$UserSession->userId.".txt";
       
        if (file_exists($filename)) {
      		$room_file=file($filename,FILE_IGNORE_NEW_LINES);

	        if( $room_file ) { 
	            for($line=0;$line<count($room_file);$line++){
	                $messageArr=split("<!@!>",$room_file[$line]);
	               
	                if( trim($messageArr[4]) == trim($UserSession->userId) ){
	                    $user = 'Me';
	                }else{
	                    $user = $messageArr[3];
	                }
	                if($messageArr[0]>$lastreceived) echo "<b>".$user."</b>: ". $messageArr[5]."<br>";
	            }
	            echo "<SRVTM>".$messageArr[0]; die();
	        }
	    }else{

	    	exit();
	    }
       
        $this->view->data = "";         
    }

    public function sendAction()
    {        //	echo "<pre>"; print_r($message); die;
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
        $chatWith = $_POST['receiver'];
        $sql = 'SELECT email, first_name, last_name FROM user WHERE id=?';
        $result = $db->fetchAll($sql, $UserSession->userId, 2);
       
        $message=strip_tags($_POST['message']);
        $message=stripslashes($message);

        $filename="./chat/room_".$chatWith."_".$UserSession->userId.".txt";
        if (file_exists($filename)) {
	        $existsfile = "./chat/room_".$chatWith."_".$UserSession->userId.".txt"; 
	    }else{
			$existsfile = "./chat/room_".$UserSession->userId."_".$chatWith.".txt";
	    }  
    	$room_file=file($existsfile ,FILE_IGNORE_NEW_LINES);
        $room_file[]=time()."<!@!>".$chatWith."<!@!>".date("Y-m-d H:i:s"). "<!@!>".$result[0]['first_name']."<!@!>". $UserSession->userId . "<!@!>".$message;
        if (count($room_file)>20)$room_file=array_slice($room_file,1);
        $file_save=fopen($existsfile ,"w+");
        flock($file_save,LOCK_EX);
        for($line=0;$line<count($room_file);$line++){
            fputs($file_save,$room_file[$line]."\n");
        };
        flock($file_save,LOCK_UN);
        fclose($file_save);
        echo "sentok";
        exit();
    }

    //@jeevan front end fetch all user transactions of the logged in user
    public function budgetplannerAction() {
	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// posting budget calculator form
	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM contacts WHERE referer = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
	}
	
	// posting budget calculator form
	if($this->getRequest()->isPOST()) {
		    if(isset($UserSession->userId)){		
			$actualCost =  (isset($_POST['actualCost'])) ? $_POST['actualCost'] : 0;
			$services =  (isset($_POST['services'])) ? $_POST['services'] : 0;
			
			$result = $db->fetchAll("select count(*) from budget_planner where booking_id=?", array($_POST['bookingId']), 2);
			if( isset($services) ) {
			    //if( $result[0]['count(*)'] ) {				
				foreach( $services as $service => $serve ) {				 		    
				    $data = array(
						  'user_id'=>$UserSession->userId,
						  'booking_id'=>$_POST['bookingId'],
						  'service_id'=>$serve,
						  'estimated_cost'=>$_POST['estimatedCost'][$service],
						  'actual_cost'=>$_POST['actualCost'][$service],
						  'paid_amount'=>$_POST['payment'][$service],
						  'pending_amount'=>$_POST['balanceDue'][$service],
						  'added_on'=>date("Y-m-d H:i:s"),
						  'updated_on'=>date("Y-m-d H:i:s")
						  ); 
				    if( !$result[0]['count(*)'] ) {
					$db->insert('budget_planner', $data);
				    } else {					
					$db->update('budget_planner', $data, 'service_id='.$serve.' and booking_id='.$_POST['bookingId'].'');
				    }			    
				}				
			    //}
			}			
		    }
	}	
    }
    
    //@jeevan add new service to the budget planner
    public function addnewserviceAction() {
	
	$db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	$request = new Zend_Controller_Request_Http;
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
	try {
	    
	    // request to get simple form adding values into the service list
	    if($this->getRequest()->isGET()) {
		$id=$request->get('id');
		$service = $db->fetchAll("select * from vendor as v, vendor_category as vc where v.category not in (select service_id from budget_planner where booking_id=?) and v.category=vc.id", array($id), 2);
		$this->view->data = array('method'=>$this->getRequest()->getMethod(),'booking_id'=>$id,'data'=>$service);	    
	    }
	    
	    // request to get simple form adding values into the service list
	    if($this->getRequest()->isPOST()) {
		
		$additionalService = $this->getRequest()->getPost('additionalService', null);
		$customService = $this->getRequest()->getPost('customService', null);
		$bookingId = $this->getRequest()->getPost('booking_id', null);
		
		if( $bookingId ) {    
		    // Additional already existing services into budget planner
		    if( $additionalService ) {
			
			$add = $db->fetchAll("select * from vendor where category=?", array($additionalService[0]), 2);
			
			$data = array(
				      'user_id'=>$UserSession->userId,
				      'booking_id'=>$bookingId,
				      'service_id'=>$add[0]['category'],
				      'estimated_cost'=>$add[0]['service_cost'],
				      'actual_cost'=>$add[0]['service_cost'],
				      'added_on'=>date("Y-m-d H:i:s"),
				      'updated_on'=>date("Y-m-d H:i:s"),
				      );
			if( $add ) {
			    $db->insert("budget_planner", $data);
			}
			
		    }
		    
		    // Adding custom service to budget planner
		    if( $customService ) {		    
			$estimated_cost = $this->getRequest()->getPost('estimatedCost', null);
			$actual_cost = $this->getRequest()->getPost('actualCost', null);
			
			if( !is_numeric( $estimated_cost ) || $estimated_cost <=0 ) {
			    
			    print(" Invalid estimated cost !! "); exit;
			}
			
			if( !is_numeric( $actual_cost ) || $actual_cost <=0 ) {
			    
			    print(" Invalid actual cost !! "); exit;
			}
			
			// creating new service category not available in app
			$data = array('category_name'=>$customService,'added_on'=>date("Y-m-d H:i:s"),'updated_on'=>date("Y-m-d H:i:s"));
			
			if( !$db->fetchAll("select * from vendor_category where category_name=?", array($customService), 2 ) ) {
			
			    if( $db->insert("vendor_category", $data) ){ // check if service category goes in database
				
				$cat_id = $db->lastInsertId('vendor_category'); // fetch the last inserted category id for vendor entry
				if( $cat_id ) {
				    if( $db->insert("vendor", array('category'=>$cat_id, 'service_cost'=>$estimated_cost)) ) {
					$vendor_id = $db->lastInsertId('vendor'); // fetch the last inserted vendor id for vendor entry
					// creating new vendor now
					$data = array(
						  'user_id'=>$UserSession->userId,
						  'booking_id'=>$bookingId,
						  'service_id'=>$cat_id,
						  'estimated_cost'=>$estimated_cost,
						  'actual_cost'=>$actual_cost,
						  'added_on'=>date("Y-m-d H:i:s"),
						  'updated_on'=>date("Y-m-d H:i:s"),
						  );
					
					if( $data ) {
					    $db->insert("budget_planner", $data);
					    echo "Success, you have imported vendor service and added custom vendor service to budget planner !!!";
					} else {
					    echo "Error, please try again !";
					}
					
				    }
				}
				
			    }
			
			} else { echo "Success, you have imported vendor service, failure the custom vendor service already exist !!!"; }
		    }
		    
		} else { echo "Budget planner is empty, please select you budget event first or create new event booking !!!"; }
	    }
	} catch (Exception $e) {
            // handle exceptions yourself
            echo $e;
        }	
    }
    
    //@jeevan front end fetch all user transactions of the logged in user
    public function budgetcalculatorAction() {
	
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}	
	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM contacts WHERE referer = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    
			    $query = $db->select()
				    ->from(array('v' => 'vendor'), array('*'))
				    ->join(array('vc' => 'vendor_category'), 'v.category = vc.id', array('*'))
				    ->where('v.public=?', array('0'), 2);
				    
			    //echo "<pre>";
			    $resultMedia = $db->fetchAll($query);
			    //print_r($resultMedia); die;
			    
                            if($resultMedia){
				$result = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('data' => $result);
		    }
	}
	
	// posting budget calculator form
	if($this->getRequest()->isPOST()) {
		    print_r(" In post request"); die();
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM contacts WHERE referer = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    
			    $query = $db->select()
				    ->from(array('v' => 'vendor'), array('*'))
				    ->join(array('vc' => 'vendor_category'), 'v.category = vc.id', array('*'))
				    ->where('v.public=?', array('0'), 2);
				    
			    //echo "<pre>";
			    $resultMedia = $db->fetchAll($query);
			    //print_r($resultMedia); die;
			    
                            if($resultMedia){
				$result = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('data' => $result);
		    }
	}
    }

    //@jeevan front end budget payment handler 
    public function budgetpaynowAction() {
	
        $db=Zend_Registry::get("db");
	$request = new Zend_Controller_Request_Http;
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	if( $request->isGet() ) {
	    $id = $request->get('service_id');$b_id = $request->get('booking_id');
	    $this->view->data = array('method'=>$this->getRequest()->getMethod(),'service_id'=>$id,'booking_id'=>$b_id);
	}
	
	if( $request->isPost() ) {
	    $data = array(
		'payee'=>$_POST['payee'],
		'amount'=>$_POST['amount'],
		'pay_type'=>$_POST['payType'],
		'date'=>date("Y-m-d H:i:s"),
		'next_pay_date'=>date("Y-m-d H:i:s"),
		'service_id'=>$_POST['service_id'],
		'booking_id'=>$_POST['booking_id'],
	    );
	    $n = $db->insert('budget_payment', $data);
	    if( $n ) {
		$this->view->data = array('method'=>$this->getRequest()->getMethod(),'status'=>'success');
	    } else {
		$this->view->data = array('method'=>$this->getRequest()->getMethod(),'status'=>'failure');
	    }	    
	}
    }

    //@jeevan front end budget payment handler 
    public function myjobsAction() {
	
        $db=Zend_Registry::get("db");
	$request = new Zend_Controller_Request_Http;
	$UserSession = new Zend_Session_Namespace('UserSession');
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// @jeevan list all the jobs created by the user
	try {	    
	    //@ handle the main get list all jobs created by me, sorted by recent on top
            if( $request->isGet() ) {
		// select * from job where user_id=? order by added_on
                $data = $db->fetchAll("SELECT count(j.id)  countvalue , j.id as job , j.*, a.* FROM job j left join appliers a  on j.id =a.job_id where j.user_id=? GROUP BY j.id ", array($UserSession->userId), 2);
		//echo "<pre>"; print_r($data);die;
		 $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
                if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=> $data ,'media' => $media);
                }else{
		    $this->view->data = array('media' => $media);
		}
                
            }
            
            // @ handle the main post request of the controller method
            if( $request->isPOST() ) {
                
                echo "In post request"; die;
                
            }	    
	} catch ( Exception $e ) {
	    
	}
    }
    
    //@jeevan front end budget payment handler 
    public function jobseekersAction() {
	
        $db=Zend_Registry::get("db"); // define standard database object 
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	$UserSession = new Zend_Session_Namespace('UserSession'); // decleare front end user session scope
	
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams(); // make array the get all request parameters
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	// @jeevan list all the jobs created by the user
	try {	    
	    // @jeevan get the list of my jobs created by me
	    if( $request->isGET() ) {
		echo $params['job']; 
		$data = $db->fetchAll("select * from appliers as ap, vendor as v where ap.job_id=? and ap.applier_id=v.user_id order by added_on", array( $params['job'] ), 2);
		 $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
		if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data,'media'=>$media );
                    //echo "<pre>"; print_r($this->view->data); die;
                }else{
		    $this->view->data = array( 'media'=>$media );
		    }		
	    }
	    
	    // @jeevan get the list of my jobs created by me
	    if( $request->isPOST() ) {
		
	    }	    
	} catch ( Exception $e ) {
	    
	}
    }
    public function appliersAction() {
	
        $db=Zend_Registry::get("db"); // define standard database object 
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	$UserSession = new Zend_Session_Namespace('UserSession'); // decleare front end user session scope
	
	
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}

	// @jeevan list all the jobs created by the user
	try {	    
	    // @jeevan get the list of my jobs created by me
	    if( $request->isGET() ) {
		
		$data = $db->fetchAll("SELECT  v.id ,v.first_name,v.email,v.about,ap.applier_id,ap.job_id,jb.title FROM appliers AS ap INNER JOIN vendor AS v ON ap.applier_id = v.user_id INNER JOIN job AS jb ON jb.id = ap.job_id WHERE jb.user_id = ? ",array($UserSession->userId));
		 $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
		if ( $data ) {
                    // forward data to the view
                    $this->view->data = array( 'data'=>$data,'media'=>$media );
                    //echo "<pre>"; print_r($this->view->data); die;
                }else{
		    $this->view->data = array( 'media'=>$media );
		    }		
	    }
	    
	    // @jeevan get the list of my jobs created by me
	    if( $request->isPOST() ) {
		
	    }	    
	} catch ( Exception $e ) {
	    
	}
    }
    //@jeevan front end budget payment handler 
    public function settingsAction() {
	
        $db=Zend_Registry::get("db"); // define standard database object 
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	$UserSession = new Zend_Session_Namespace('UserSession'); // decleare front end user session scope
	
	$params = Zend_Controller_Front::getInstance()->getRequest()->getParams(); // make array the get all request parameters
	if( !isset( $UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
	try {
	    
	    // @list all account information of the user in horigontal form
	    if( $request->isGET() ) {
		$data = $db->fetchAll("select * from user where id=?", array($UserSession->userId), 2); // fetch all information of user from database
		  $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
		$this->view->data = array( 'data'=>$data ,'media' => $media);
		
	    }
	    
	    // @user posting data and his account settings info
	    if( $request->isPOST() ) {		
		try {		    
		    // @ check if phone number is valid
		    if( !is_numeric($_POST['phone']) ) {
			print_r(" Phone number is invalid !!! "); exit;
		    }
		    
		    // @ check if phone number is valid
		    if( strlen($_POST['phone']) <> 10 ) {
			print_r(" Phone number must be 10 integer !!! "); exit;
		    }
		    
		    // @ check if phone number is valid
		    if( !is_numeric($_POST['pincode']) ) {
			print_r(" Pin code is invalid !!! "); exit;
		    }
		    
		    // @ check if phone number is valid
		    if( !is_numeric($_POST['age']) ) {
			print_r(" Age is invalid !!! "); exit;
		    }
		    
		    $data = array(
                                'first_name' => $_POST['first_name'],
                                'last_name' =>  $_POST['last_name'],
				'date_of_birth' => $_POST['date_of_birth'],
				'gender' => $_POST['gender'],
				'age' => $_POST['age'],
				//'bio' => mysql_real_escape_string($_POST['bio']),
				'bio' => $_POST['bio'],
				'address' => $_POST['address'],
				'pincode' => $_POST['pincode'],
				'phone' => $_POST['phone'],
				'email' => $_POST['email'],
				'fb_name' => $_POST['fb_name'],
				'tw_name' => $_POST['tw_name'],
				'sky_name' => $_POST['sky_name'],
				'gplus_name' => $_POST['gplus_name']
                            );
		    //print_r($data); die;
		    $n = $db->update('user', $data, 'id = '.$UserSession->userId.'');
		    
		    if( $n ) {
			
			print ( "Account information updated successfully" ); exit;
			
		    } else {
			print ( "Error, unable to update your account, Kindly if you filled all fields !!" ); exit;
		    }
		    
		} catch (Exception $e) {
		    // handle exceptions yourself
		    echo $e;
		}
		
	    }
	    
	} catch ( Exception $e ) {
	    
	    print $e; exit;
	    
	}
    }

     public function deactivateAction(){
	
	$UserSession = new Zend_Session_Namespace('UserSession'); // decleare front end user session scope
	if( !isset( $UserSession->userId ) ){
	     $this->_helper->json(array('status' => 'error'));
	     return;
	}
	
	$db = Zend_Registry::get("db"); // define standard database object 
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	try {
		if( $request->isGET() ) {
			$data = array("status" => 0);
			$n = $db->update('user', $data, 'id = '.$UserSession->userId.'');
			if($n){ 			
				unset($UserSession->userId);
				unset($UserSession->userStatus);
				unset($UserSession->userRole);
				$this->_helper->json(array('status' => 'success'));
	     			return;
			}else{
				$this->_helper->json(array('status' => 'error'));
	     			return;
			}			
			
		}
	} catch (Exception $e) {
	    // handle exceptions yourself
	    echo $e;exit;
	}	
	
	
    }

    // @ By deepak to get list of all contacts of current logged in user
    public function mycontactsAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	// @ exception handler
	try {	    
	    // @ get list of all contacts
	    if($this->getRequest()->isGET()) {
		$gmail = $db->fetchAll('SELECT * from contacts where referer = ? and type = ?', array($UserSession->userId, 'Gmail'), 2); 
		$facebook = $db->fetchAll('SELECT * from contacts where referer = ? and type = ?', array($UserSession->userId, 'facebook'), 2); 
		$yahoo = $db->fetchAll('SELECT * from contacts where referer = ? and type = ?', array($UserSession->userId, 'yahoo'), 2);
		$msn = $db->fetchAll('SELECT * from contacts where referer = ? and type = ?', array($UserSession->userId, 'Msn'), 2);     
		
		$usersection = 'user-avatar';
		$resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                if($resultMedia){
		    $media = $resultMedia ;
		    }else {
		    $media = '';
		    }
		$this->view->data = array('gmail' => $gmail,'facebook' => $facebook,'yahoo' => $yahoo,'msn' => $msn,'media' => $media);    
	    }	    
	} catch ( Exception $e ) {	    
	    print $e; exit;	    
	} 
    }
    
    // @ jeevan feedback and reviews by front end users
    public function reviewsAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	// @ exception handler starts
	try{
	 
	    // @ in get template list all reviews and feedbacks
	    if($this->getRequest()->isGET()) {
		
		$data = $db->fetchAll("select * from feedback_reviews where status=? AND about_who=? order by id DESC", array(0,'site'), 2);
		 $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
		if( $data ) {		 
		    $this->view->data = array('data'=>$data,'media'=>$media); // forward data to views in order to display on webpage		    
		} else {
		    $this->view->data = array('data'=>'','media'=>$media);    
		}		
	    }
	    
	    // @ in get template list all reviews and feedbacks
	    if($this->getRequest()->isPOST()) {
		// @ get the all post variables
		$feedback = $this->getRequest()->getPost('feedback');
		$about_who = $this->getRequest()->getPost('about_who');
		
		// @validation check on content coming in feedback
		if( $feedback =='' ) {
		    echo " Kindly fill some content in your feedback !! "; exit;
		}
		$data = array(
			      'user_id'=>$UserSession->userId,
			      'about_who'=>$about_who,
			      'feedback_review'=>$feedback,
			      'added_on'=>date("Y-m-d H:i:s"),
			      'updated_on'=>date("Y-m-d H:i:s")
			      );
		if( $data ) {
		    if( $db->insert("feedback_reviews", $data) ) {
			print ("Thank you, we had your feedback."); exit;
		    };
		} else {
		    print (" Error, please try again !! "); exit;
		}
	    } 
	    
	} catch( Exception $e ) {
	    
	} 
    }
    
    // @ jeevan delete review
    // @ jeevan feedback and reviews by front end users
    public function deletereviewAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	$id = $this->getRequest()->getPost('id');
	if( $id ) {
	    if( $db->delete("feedback_reviews","id=".$id."") ) {
		print("Feedback removed, successfully !!"); exit;
	    }
	}
    }
    
    // @ jeevan favourite actions
    public function favoriteAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	$case = $this->getRequest()->get('case');
	$vendor_id = $this->getRequest()->get('vendor_id');
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {
		if( $case && $vendor_id ) {
		    
		    // @ forward content to the ajax view
		    $this->view->data = array('case'=>$case, 'vendor_id'=>$vendor_id);
		    
		} else {
		    // @ forward content to the ajax view
		    $this->view->data = array('case'=>'', 'vendor_id'=>'');
		}
	    }
	    
	    // @ In post request method
	    if( $request->isPost() ) {
		
	    }
	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }
    
    // @ jeevan events handler
    public function eventsAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {
		
		$data = $db->fetchAll("select * from bookings where user_id=?", array($UserSession->userId), 2);
		$usersection = 'user-avatar';
		$resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                if($resultMedia){
		$media = $resultMedia ;
		 }else {
		    $media = '';
		}
		if( $data ) {		    
		    // @ forward content to the ajax view
		    $this->view->data = array('data'=>$data,'media'=> $media);
		    
		}else{
		    $this->view->data = array('media'=> $media);
		}
	    }
	    
	    // @ In post request method
	   	if($this->getRequest()->isPost()) {
	   

		$userGroup          = (isset($_POST['user_group'])) ? $_POST['user_group'] : '';
		$userdate           = (isset($_POST['user_date'])) ? $_POST['user_date'] : '';
		$religion           = (isset($_POST['religion'])) ? $_POST['religion'] : '';
		$ceremony           = (isset($_POST['ceremony'])) ? $_POST['ceremony'] : '';
		$damas              = (isset($_POST['damas'])) ? $_POST['damas'] : '';
		$chambelanes        = (isset($_POST['chambelanes'])) ? $_POST['chambelanes'] : '';
		$weddingDress       = "#".$_POST['dresscolor1'].",#".$_POST['dresscolor2'];
		$weddingTheme       = "#".$_POST['themecolor1'].",#".$_POST['themecolor2']; 
		$quinceType         = (isset($_POST['quince'])) ? $_POST['quince'] : '';
		$guests             = (isset($_POST['guests'])) ? $_POST['guests'] : '';
		$spentmoney         = (isset($_POST['budget'])) ? $_POST['budget'] : '';
		$moneysofor         = (isset($_POST['moneysofor'])) ? $_POST['moneysofor'] : '';
		
		$optionaData = array(	
		  'user_id'              => $UserSession->userId,
		  'user_group'           => $userGroup,
		  'user_date'            => $userdate,
		  'religion'             => $religion,
		  'ceremony'             => $ceremony,
		  'damas'                => $damas,
		  'chambelanes'          => $chambelanes,
		  'wedding_dress'        => $weddingDress,
		  'wedding_theme'        => $weddingTheme,
		  'quince_type'          => $quinceType,
		  'guests'               => $guests,
		  'event_money'          => $spentmoney,
		  'saved_so_for'         => $moneysofor
		  
		  
		  
		);
		
		// $data = array('method'=>$this->getRequest()->getMethod(), 'data'=> 'Optional data has been saved successfully.', 'resp'=> 'success');
		
		    $val = $db->update('bookings', $optionaData, 'user_id ='.$UserSession->userId);	
    
		if ( $val  ) {
		    
		    print("We have received your information successfully !!"); exit;
			
		} else {
		   print("We have not received any information about your event, kindly fill out all the required fields in event list"); exit;
			//$this->view->message = "We have not received any information about your event, kindly fill out all the required fields in user steps.";
		}
	    } else {
		echo "Not connected".mysql_error();
	    } 
	}
	    catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }
    
    // @ jeevan events edit handler
//    public function eventseditAction() {
//    	$db=Zend_Registry::get("db");
//        $UserSession = new Zend_Session_Namespace('UserSession');
//
//        if( !isset($UserSession->userId ) ){
//            $this->_redirector->gotoSimple('index', 'index' , null );
//        }	
//	$request = new Zend_Controller_Request_Http; // initilize the http request object
//	
//	// @ exception handler
//	try {
//	    // @ In get request method
//	    if( $request->isGet() ) {
//		
//		$data = $db->fetchAll("select * from bookings where user_id=? and id=?", array($UserSession->userId), 2);
//		if( $data ) {		    
//		    // @ forward content to the ajax view
//		    $this->view->data = array('data'=>$data);
//		    
//		}
//	    }
//	    
//	    // @ In post request method
//	    if( $request->isPost() ) {		
//		$case = $this->getRequest()->get('id');
//		$user = $this->getRequest()->get('user');
//	    }
//	    
//	} catch( Exception $e ) {
//	    print ($e); exit;
//	}
//	
//    }
    
    // @ jeevan delete events
    public function deleteeventAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	//@ exception handler
	try{
	    if( $request->isPOST() ) {
		$id = $this->getRequest()->getPost('id');
		if( $id ) {
		    if( $db->delete("bookings","id=".$id."") ) {
			print("Event removed, successfully !!"); exit;
		    } else {
			print("Error, please try again later !!!"); exit;
		    }
		}
	    }	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
    }
    
    // @ jeevan events handler
    public function guestlistAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {		
		$data = $db->fetchAll("select * from households", array(), 2);
		 $usersection = 'user-avatar';
		$resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
                if($resultMedia){
		$media = $resultMedia ;
		 }else {
		    $media = '';
		}
		if( $data ) {		    
		    // @ forward content to the ajax view
		    $this->view->data = array('data'=>$data,'media'=>$media);
		    
		}else{
		    $this->view->data = array('media'=>$media);
		}
	    }
	    
	    // @ In post request method
	    if( $request->isPost() ) {
		echo "Jeevan"; die; 
		$case = $this->getRequest()->get('id');
		$user = $this->getRequest()->get('user');
	    }
	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }
    
    // @ jeevan guestlist operation handler
    public function guestlistoperationAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null);
        }	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {
		// @ forward content to the ajax view
		$this->view->data = array('method'=>$request->getMethod(), 'data'=>$data, 'user'=>$UserSession->userId);
	    }	    
	    // @ In post request method
	    if( $request->isPost() ) {
		echo "Jeevan"; die; 
		$case = $this->getRequest()->get('id');
		$user = $this->getRequest()->get('user');
	    }
	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }

	//**************** vendor revies by a user 
    public function vendorreviewAction(){
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('UserSession');
		    try{		    		    

			    if($this->getRequest()->isPOST()) {

					$feedback = $this->getRequest()->getPost('feedback');
					$about_who = $this->getRequest()->getPost('about_who');
					
					if( $feedback =='' ) {
					    echo " Kindly fill some content in your feedback !! "; die;
					}
					$data = array(
						      'user_id'=>$UserSession->userId,
						      'about_who'=>$about_who,
						      'feedback_review'=>$feedback,
						      'added_on'=>date("Y-m-d H:i:s"),
						      'updated_on'=>date("Y-m-d H:i:s")
						      );
					if( $data ) {
					    if( $db->insert("feedback_reviews", $data) ) {
						print ("Thank you, we had your feedback."); die;
					    };
					} else {
					    print (" Error, please try again !! "); die;
					}
			    }
		    	die("vendor revie action");

		    }catch (Exception $e){

		    }

    }

}
