<?php

class Vendor_IframeController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init()
    {
        $this->_helper->layout->setLayout('iframe');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        
    }

    public function indexAction() {
        $this->view->data = NULL;

    }

}

