<?php

class Vendor_SearchController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
	$ajaxContext->addActionContext('vsearch', 'html')
	            ->addActionContext('aep', 'html')
		    ->addActionContext('as', 'html')
	            ->initContext();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function vsearchAction() {
        
        $request = new Zend_Controller_Request_Http;
        if ( $request->isPost() ) {
            try {
                $country = $this->getRequest()->getPost('country', null);
                $cat 	= $this->getRequest()->getPost('category', null);
                $keyword = $this->getRequest()->getPost('search_key', null);
                $rating = $this->getRequest()->getPost('rating', null);
		        $state = $this->getRequest()->getPost('state', null);
                $db=Zend_Registry::get("db");
                //foreach($cat as $category) {
                //    
                //    $query .=" AND category = $category"; 
                //    
                //}
		if($cat){
		  $q=implode("','",$cat);
		}else{
		    $q='';
		}
		if($q){
		  $query="AND category In ('".$q."')";
		}else{
		  $query ='';    
		}
                if( $keyword ) {
                    $key = " AND first_name LIKE '%$keyword%' OR last_name LIKE '%$keyword%' OR address LIKE '%$keyword%' OR about LIKE '%$keyword%' OR zipcode LIKE '%$keyword%'";
                } else {
                    $key ="";
                }                
                if( $country ) {
                    $country = " AND country ='$country'";
                } else {
                    $country = "";
                }
                if( $state ) {
                    $state = " AND state ='$state'";
                } else {
                    $state = "";
                }
		        if( $rating ) {
                    $rate = " AND rating ='$rating'";
                } else {
                    $rate = "";
                }
                $sql = $country.$rate.$query.$key.$state; 
                // /echo "<pre>"; print_r($sql) ; die;
                $result = $db->fetchAll("SELECT * FROM vendor where public=? $sql", array('1'), 2);
                if ( !$result ) {
                    //print("User with this email and password does not exist !");
                } else {
                    $this->view->data = array('vendor'=>$result);
                }
            } catch (Exception $e) {
                // handle exceptions yourself
                echo $e;
            }
        }
        
    }


}

