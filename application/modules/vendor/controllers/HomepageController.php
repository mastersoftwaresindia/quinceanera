<?php

class Vendor_HomepageController extends Zend_Controller_Action
{  
    protected $_redirector = null;

    public function init()
    {	
    	$_SESSION['count'] = 1;
        $this->_helper->layout->setLayout('vendorhomepage');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('avp', 'html')
        			->addActionContext('rlv', 'html')
        			->addActionContext('viewedby', 'html')
                    ->initContext();
    }

    public function viewedbyAction(){

    	$db=Zend_Registry::get("db");
    	$remote_addr = $_SERVER["REMOTE_ADDR"];
    	//$remote_addr = "112.196.35.196";
    	$vendor_id = $_POST['vendor_id'];
    if($vendor_id)
    {	
    	try{
	    	$select = $db->select()
				    	->from('viewed_by')
				    	->where('vendor_id = ?', $vendor_id);
			$getData = $db->fetchAll($select);	  
				if(count($getData)){ // when record is present for the user

					$select_ip = $db->select()
									->from('viewed_by')
									->where('ip = ?',$remote_addr)
									->where('vendor_id = ?',$vendor_id);
					$getData_ip = $db->fetchAssoc($select_ip);

					foreach($getData_ip as $mainData);
					
						if(count($getData_ip)){ // if ip address already present and vendor id is also present 
							$count = $mainData['count']; 
							$count ++;
							$id = $mainData['id'];
							$ip_data = array(
									'count' => $count,

								);
							$update = $db->update('viewed_by',$ip_data,array("id = $id"));

						
						}else{ // if ip not present but vendor id is present 

								$data = array(
									'ip' => $remote_addr,
									'count' => 1,
									'vendor_id' => $vendor_id
									);
								$db->insert('viewed_by',$data);
		
						}

				}else{ // when no record is present for the user 

					$data = array(
						'ip' => $remote_addr,
						'count' => 1,
						'vendor_id' => $vendor_id
						);
					$db->insert('viewed_by',$data);
					
				}

			}catch(Exception $e ){
				//echo "<pre>"; print_r($e);die();
			}	  	
	}		

    }

    public function indexAction() {
		
    $request = new Zend_Controller_Request_Http;
    $db=Zend_Registry::get("db");
	$VendorSession = new Zend_Session_Namespace('VendorSession');
	$UserSession = new Zend_Session_Namespace('UserSession');

	$request2 = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request2->getParams();

    	if($UserSession->userId)
    		{$vendorMenuFlag = "true";}
        elseif($VendorSession->userId)
        	{$vendorMenuFlag = "false";}
    	else
    		{$vendorMenuFlag = "false";}
		
     	if( $this->getRequest()->isGET() ) {

		if( $params['id'] ) {		

			$vendor_image = $db->fetchAll('SELECT * FROM media WHERE owner= ? AND section = "user-avatar" ', array( $params['id'] ), 2);		
			$videos = $db->fetchAll("select * from media where owner=? and section in ('vendorvideo', 'vendorvideoimage') and status=?", array($params['id'] , '1'), 2);
			$vendor_info = $db->fetchAll('select * from vendor where user_id= ?', array( $params['id'] ), 2);		
			$vendor = $db->fetchAll("select * from vendor where public=?", array('1'), 2);
			$viewed_by = $db->fetchAll("select SUM(count) as totalviews from viewed_by where vendor_id=?", array($params['id']), 2);
			$viewed_by_count = $viewed_by[0]['totalviews'];
			$gallery = $db->fetchAll('select * from media where owner=? and section=? and status = 1', array($params['id'] , 'vendorgallery'), 2);
			$gallery2 = $db->fetchAll('select * from media where owner=? and section=? and status = 1', array($params['id'] , 'vendor_image'), 2);
			$albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($params['id']),2); 
			$review = $db->fetchAll("select * from feedback_reviews as r,user as u where r.user_id=u.id AND r.status=? AND r.about_who=? order by r.id DESC", array(0,$params['id'] ), 2);

		}
		//echo "<pre>"; print_r($vendor_image); die("here");
		$this->view->data = array('videos'=>$videos, 'images' => $gallery ,'vendor_re'=>$vendor, 'vendor' => $vendor_info,'vendorMenuFlag'=>$vendorMenuFlag ,'viewed_by'=>$viewed_by_count, "vendor_image"=>$vendor_image,"review"=>$review,"singleimages"=>$gallery2,'albums'=>$albums);		

	}

//form vendor request callback/quote        
	  $this->view->assign('action',"homepage");
	  $this->view->assign('label_cellno_callback_radio',' Request Callback');
	  $this->view->assign('label_cellno_callback_radio_quote',' Quote');
	  $this->view->assign('title_callback',' ');
	  $this->view->assign('label_cellno_callback','Enter your Cell Phone Number :');
	  $this->view->assign('label_event_info','Event Info :');  				
	  $this->view->assign('label_eventdate_callback','Date :');  
	  $this->view->assign('label_guest_callback','Guest :');  
	  $this->view->assign('label_comment_callback','Comment :');
	  $this->view->assign('label_submit_callback','Send   Request');      
	  $this->view->assign('description','Please enter this form completely:');
	  
//form vendor request appointment        
	  $this->view->assign('action',"homepage");
	  $this->view->assign('title',' ');
	  $this->view->assign('label_event_info_appoin','Event Info :');  	  
	  $this->view->assign('label_eventdate','Event Date :');  
	  $this->view->assign('label_guest','Guest :');  
	  $this->view->assign('label_comment','Comment :');
	  $this->view->assign('label_submit','Send   Request');      
	  $this->view->assign('description','Please enter this form completely:');

//form Message	  
	  $this->view->assign('messageaction',"homepage");
	  $this->view->assign('title_message','Vendor Message');
	  $this->view->assign('label_message',' ');
	  $this->view->assign('label_submit_message','Send   Message');      
	  $this->view->assign('description','Please enter this form completely:');

//form Review	  
	  $this->view->assign('reviewaction',"homepage");
	  $this->view->assign('title_review','Vendor Review');
	  $this->view->assign('label_review',' ');
	  $this->view->assign('label_submit_review','  Send  ');      
	  $this->view->assign('description','Please enter this form completely:'); 	  
	
    }

 public function avpAction(){
	
        $db=Zend_Registry::get("db");
      
	$VendorSession = new Zend_Session_Namespace('VendorSession');
	$UserSession = new Zend_Session_Namespace('UserSession');
	$user_id = $UserSession->userId;
	$vendor_id = $_POST['vendor_id'];
	$data_convo = array('name'=>'Sent From Vendor\'s Profile Page','with_who'=>$vendor_id,'by_who'=>$user_id);
	$db->insert('conversation', $data_convo);
	$last_id = $db->lastInsertId();

		if($user_id){
		    $data = array(
		    		'conv_id' => $last_id,
                    'sent_to' => $vendor_id,
                    'by_from' => $UserSession->userId,
                    'message' => $_POST['Msg'],
                    'date'    => date("Y-m-d H:i:s"),
	                        );     
	         $n = $db->insert('messages', $data);
	          if( $n ) {
	                      echo "Send Message";
		}
	        
		}else{
			echo  $errmsg="Please Login Your account first";
	    }

    }
    //****************** to show related vendors on the 
   public function rlvAction(){
   		$UserSession = new Zend_Session_Namespace('UserSession');
   		$db=Zend_Registry::get("db");
   		$offset = $_POST['count'];
   		//echo($offset);
   		$vendor_id = $_POST['vendor_id']; 
   		$vendor_category = $db->fetchAll('select category from vendor where user_id = ?', array($vendor_id), 2);
   		$vendor_category = $vendor_category[0]['category'];
   		if($offset>0){$offset++;}
   		
	    $countries = $db->fetchAll('select * from countries order by country_id DESC', array(), 2);
	    $cities = $db->fetchAll('select * from cities order by cid DESC', array(), 2);
	    $favorite = $db->fetchAll('select favorite_id from favorites where user_id=?', array($UserSession->userId));
	    $favrt = array();
	    if($favorite){
			foreach($favorite as $key => $value){
			    array_push($favrt,$value->favorite_id);
			}
	    }
   		$graphic = $db->fetchAll('select * from media where section=? order by id DESC', array('user-avatar'), 2);
      	//$vendor = $db->fetchAll("select * from vendor where public=?", array('1'), 2);
      	$select = $db->select()->from('vendor')->where('category = ?', $vendor_category)->where('user_id != ?', $vendor_id)->limit(2, $offset);
      	$vendor = $db->fetchAssoc($select);
      	//echo "<pre>"; print_r($vendor); die;
   		$this->view->data = array('vendor_re'=>$vendor,'graphic'=>$graphic,'cities'=>$cities,'countries'=>$countries,'favrt'=>$favrt);

   }
    
//*********************************************** AngularJs fucntions Start
//public function vendorinfoAction(){
    /*	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
	$VendorSession = new Zend_Session_Namespace('VendorSession');
	$user_id = $VendorSession->userId;
	$request2 = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request2->getParams();

		if( $this->getRequest()->isGET() ) {
			$user_id =	$params['id'];
		}
		if( $user_id  ) {		
			$gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id , 'vendorgallery'), 2); 
			$vendor_info = $db->fetchAll('select * from vendor where user_id= ?', array( $user_id  ), 2);		
		}
	
		$vendorData = array( 'images' => $gallery , 'vendor' => $vendor_info );	
		print_r(json_encode($vendorData));
		die;
*/

  //} 
    
//*********************************************** AngularJs fucntions end    
}
