<?php

class Vendor_DashboardController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init()
    {
        $this->_helper->layout->setLayout('v');
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('avp', 'html')
                    ->addActionContext('aep', 'html')
                    ->addActionContext('as', 'html')
                    ->initContext();
    }
    
    public function indexAction() {

        $request = new Zend_Controller_Request_Http;
        //get default session namespace
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }

        $db=Zend_Registry::get("db"); 
        $email = $UserSession->userEmail;
        
        $sql2 = 'SELECT * FROM user WHERE email = ?';
        $user = $db->fetchAll($sql2, $email);

        $status = $user[0]->status;
        $user_id = $user[0]->id;
        
        $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
        $category = $db->fetchAll($sql);
     
        $sql1 = 'SELECT * FROM countries';                        //fetch all countries
        $countries = $db->fetchAll($sql1);

        $sql3 = 'SELECT * FROM vendor WHERE email = ?';
        $vendor = $db->fetchAll($sql3, $email);

        $country = $vendor[0]->country;
      
        $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
        $vendor1 = $db->fetchAll($sql3, $country);
        $country_id = $vendor1[0]->country_id;
         
        $videos = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorvideo'), 2);
        $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);

        $data = array('data'=> $category,'country'=>$countries,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'country_id'=>$country_id, 'videos'=>$videos, 'gallery'=>$gallery);
          
        $this->view->data = $data;
        if($request->isPost()) {

            try{  
               
                $user_data = array('first_name'   => $_POST['fname'],            //insertion in user table
                             'last_name'    => $_POST['lname'],
                             'address'      => $_POST['address'],
                             'pincode'      => $_POST['zipcode'],
                             'phone'        => $_POST['phone'],
                             'website'      => $_POST['website'],
                         
                            );

                //echo "user data<pre>"; print_r($user_data); die;

                //$db->insert('user', $user_data);
                $updateUser = $db->update('user', $user_data, 'id ='.$user_id);
            
                $vendor_data = array( 'category'      => $_POST['category'],         //insertion in vendor table
                               'first_name'    => $_POST['fname'],  
                               'last_name'     => $_POST['lname'],
                               'address'       => $_POST['address'],
                               'city'          => $_POST['city'],   
                               'state'         => $_POST['state'],
                               'country'       => $_POST['country'],
                               'phone'         => $_POST['phone'],
                               'zipcode'       => $_POST['zipcode'],
                               'website'       => $_POST['website'],
                               'fax'           => $_POST['fax'],   
                               'occupation'    => $_POST['occupation'],
                               'company_logo'  => $_POST['company_logo'],
                               'company_name'  => $_POST['company_name'],
                               //'tagline'       => $_POST['tagline'],
                               //'title'         => $_POST['title'],
                               'slider_images' => $_POST['slider_images'],
                               'about'         => $_POST['about'],
                         
                         
                            );

                //echo "user data<pre>"; print_r($user_data); die;

                 //$db->insert('vendor', $vendor_data);
                 $updateUser = $db->update('vendor', $vendor_data, 'user_id ='.$user_id);
             
                 $sql2 = 'SELECT * FROM user WHERE email = ?';
                 $user = $db->fetchAll($sql2, $email);
                 //echo "<pre>"; print_r($user);die;
                 $status = $user[0]->status;
                 $user_id = $user[0]->id;
                
                 $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
                 $category = $db->fetchAll($sql);
                 //echo "<pre>"; print_r($category);die;
             
                 $sql1 = 'SELECT * FROM countries';                        //fetch all countries
                 $countries = $db->fetchAll($sql1);
                 //echo "<pre>"; print_r($countries);die;

                 $sql3 = 'SELECT * FROM vendor WHERE email = ?';
                 $vendor = $db->fetchAll($sql3, $email);
                // echo "<pre>"; print_r($vendor);die;
                 $country = $vendor[0]->country;
                 //echo $country;die;
              
                 $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
                 $vendor1 = $db->fetchAll($sql3, $country);
                 //echo "<pre>"; print_r($vendor1); 
                 $country_id = $vendor1[0]->country_id;
                //echo $country_id;die;
                 $usersection = 'user-avatar';
		 $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId , $usersection ), 2);
                
                 print_r($resultMedia);
		    if($resultMedia){
			    $media = $resultMedia ;
			    }else {
				$media = '';
			    }
                $data = array('data'=> $category,'country'=>$countries,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'country_id'=>$country_id,'media' => $media);

                //echo "<pre>";print_r($data);die;
          
                $this->view->data = $data;

            } catch (Exception $e) {
 
               echo $e;
            }
        }
    }

   public function gallAction() {
    
     $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        $user_id = $UserSession->userId;
         if( $request->isGet() ) {            
            $videos = $db->fetchAll('select * from media where owner=? and section=? and status=?', array($user_id, 'vendorvideo','1'), 2);
            $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);   
            $albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($user_id),2); 
           $gallery2 = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendor_image'), 2); 
           // echo "<pre>";
            //print_r($albums);die;
            // $ids = [];
            // if(isset($albums)){
            //     foreach ($albums as $key => $value) {
            //         $ids[] = $value->id;
            //     }
            // }
            
            // $id = implode(',', $ids);

            //$images = $db->fetchAll("SELECT  a.* FROM media a INNER JOIN ( SELECT  album_id, MAX(ID) max_ID FROM media where album_id in ($id) GROUP BY album_id order by id desc) b ON  a.album_id = b.album_id AND a.ID = b.max_ID");
           $images = $db->fetchAll("SELECT  * FROM media where section=? and owner=?",array('vendorgallery',$user_id),2);
           // print_r($images);die;

            $this->view->data = array('videos'=>$videos,'gallery'=>$gallery,'albums'=>$albums,'images'=>$images,'singleimages'=>$gallery2);
        }
        
    }

public function videosAction() {
    
        $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        $user_id = $UserSession->userId;
         if( $request->isGet() ) {            
            $videos = $db->fetchAll("select * from media where owner=? and section in ('vendorvideo', 'vendorvideoimage') ", array($user_id), 2);
            $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);   
            $albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($user_id),2); 
           $images = $db->fetchAll("SELECT  * FROM media where section=? and owner=?",array('vendorgallery',$user_id),2);
           // print_r($images);die;
            $this->view->data = array('videos'=>$videos,'gallery'=>$gallery,'albums'=>$albums,'images'=>$images);
        }
        
    }

  public function albumImagesAction() {
    
     $request = new Zend_Controller_Request_Http;
        //get default session namespace
        Zend_Session::rememberMe(604800); // Week
        $db=Zend_Registry::get("db"); 
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
        
        $user_id = $UserSession->userId; 
         if( $request->isGet() ) {  
            
            $album_id = $_GET['id'];

            $images = $db->fetchAll('select * from media where album_id=?', array($album_id)); 
           
           // echo "<pre>"; print_r($images);die;

            $this->view->data = array('user_id'=>$user_id,'images'=>$images);
        }
        

    }


public function mypicAction()
    {
        $db=Zend_Registry::get("db");
	$UserSession = new Zend_Session_Namespace('VendorSession');
	if( !isset($UserSession->userId ) ){
	    $this->_redirector->gotoSimple('index', 'index' , null );
	}
	
     	if($this->getRequest()->isGET()) {
		    if(isset($UserSession->userId)){		
			    $sql = 'SELECT * FROM user WHERE id = ?';
			    $result = $db->fetchAll($sql, $UserSession->userId);
			    $usersection = 'user-avatar';
			    $resultMedia = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array( $UserSession->userId, $usersection ), 2);
			    print_r($resultMedia);
                            if($resultMedia){
				$media = $resultMedia ;
			    }else {
				$media = '';
			    }
			    $this->view->data = array('status' => $result[0]->status, 'data' => $result, 'media' => $media);
		    }
                    echo "session expire";
	}
        
    }
	public function contactsAction()
	{	
		$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');
        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
        if($this->getRequest()->isGET()) {
            $sql = 'SELECT * from user left join favorites on favorites.user_id=user.id  where favorite_id=?';
            $result = $db->fetchAll($sql, $UserSession->userId, 2); 
            $userIds = [];
        
            $main = [];
            foreach ($result as $key => $value) {
              //array_push($main['users'],$value);
              $main['users'][$key] = $value;    
              $images = $db->fetchAll("SELECT * FROM media where owner = ?", array($value['user_id']), 2);
              $main['media'][$key] = @$images[0];
              //array_push($userIds,$value['user_id']);
               // array_push($userIds,$value->user_id);
            }

            $ids = implode(',',$userIds);
//            echo "<pre>"; print_r($main); die; 
            if ($ids) {
                echo "Inside query";
                $images = $db->fetchAll("SELECT * FROM media where owner in ($ids)", array(), 2);

            }
       
            if($result){
                $users = $result ;
            }else {
                $users = NULL;
            }
            //$this->view->data = array('users' => $users , 'media' => $images );
            $this->view->data = $main;

      } 
    }

    public function userinitAction()
    {
        //print_r($_SERVER['SERVER_NAME']); 
        // $UserSession = new Zend_Session_Namespace('UserSession');
        // $chatWith=146;
        // echo getcwd();
        // fopen("./chat/room_".$UserSession->userId."_".$chatWith.".txt", "w");
        // //$onlineusers_file=file("http://localhost/quinceanera/library/onlineusers.txt",FILE_IGNORE_NEW_LINES);
        // //$olu=join("<br>",$onlineusers_file);
        // //echo $olu;   die();
        // $this->view->data = "";
    }

    public function receiveAction()
    {
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $chatWith=$_POST['receiver'];
        $lastreceived=$_POST['lastreceived'];
        $filename="./chat/room_".$UserSession->userId."_".$chatWith.".txt";
       
        if (file_exists($filename)) {
            $room_file=file($filename ,FILE_IGNORE_NEW_LINES);
            
            if( $room_file ) { 
                for($line=0;$line<count($room_file);$line++){
                    $messageArr=split("<!@!>",$room_file[$line]);
                    if( trim($messageArr[4]) == trim($UserSession->userId) ){
                        $user = 'Me';
                    }else{
                        $user = $messageArr[3];
                    }
                    if($messageArr[0]>$lastreceived) echo "<b>".$user."</b>: ". $messageArr[5]."<br>";
                }
                echo "<SRVTM>".$messageArr[0]; die();
            }
        }else{
            exit();
        }
        $this->view->data = "";        
    }

    public function sendAction()
    {
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $chatWith = $_POST['receiver'];
        $sql = 'SELECT email, first_name, last_name FROM vendor WHERE user_id=?';
        //$sql = 'SELECT email, first_name, last_name FROM user WHERE id=?';
        $result = $db->fetchAll($sql, $UserSession->userId, 2);
	
        $message=strip_tags($_POST['message']);
        $message=stripslashes($message);
        $filename = "./chat/room_".$chatWith."_".$UserSession->userId.".txt";
        if (file_exists($filename)) {
            $existsfile = "./chat/room_".$chatWith."_".$UserSession->userId.".txt";   
        }else {
            $existsfile = "./chat/room_".$UserSession->userId."_".$chatWith.".txt";

        } 
            $room_file=file($existsfile ,FILE_IGNORE_NEW_LINES);
            $room_file[]=time()."<!@!>".$chatWith."<!@!>".date("Y-m-d H:i:s"). "<!@!>".$result[0]['first_name']."<!@!>". $UserSession->userId . "<!@!>".$message;
		
            if (count($room_file)>20)$room_file=array_slice($room_file,1);
            $file_save=fopen($existsfile ,"w+");
            flock($file_save,LOCK_EX);
            for($line=0;$line<count($room_file);$line++){
                fputs($file_save,$room_file[$line]."\n");
            };
            flock($file_save,LOCK_UN);
            fclose($file_save);
            echo "sentok";
            exit();
    }
    // @vikrant Show Vendor Reviews
    public function reviewsAction(){
        $db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }

        try{
            $result = $db->fetchAll("SELECT * FROM feedback_reviews  WHERE about_who = ? ORDER BY id DESC", array($UserSession->userId), 2);                

            if(count($result) > 0){  
                $reviews =  $result;
            }else {
                $reviews =  NULL;
            }

            $review_main = array();
            $review_new = array();
            foreach($reviews as $review){

                $result = $db->fetchAll("SELECT * FROM user WHERE id = ?", array($review['user_id']), 2);                
                $review_new['by_email']=$result[0]['email'];
                $review_new['by_name']=$result[0]['first_name']." ".$result[0]['last_name'];
                $review_new['review']=$review['feedback_review'];
                $review_new['added_on']=$review['added_on'];
                $review_new['updated_on']=$review['updated_on'];
                $review_new['id']=$review['id'];
                array_push($review_main,$review_new);
            }

            $this->view->data = array('reviews'=> $review_main);
        }
        catch(Exception $e){

        }
    }

    // @ jeevan favourite actions
    public function bidsAction() {
    	$db=Zend_Registry::get("db");
        $UserSession = new Zend_Session_Namespace('VendorSession');

        if( !isset($UserSession->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , null );
        }
	
	$request = new Zend_Controller_Request_Http; // initilize the http request object
	
	try {
	    // @ In get request method
	    if( $request->isGet() ) {
		$result = $db->fetchAll("SELECT appliers.id ,appliers.applier_id, appliers.job_id, appliers.added_on, appliers.updated_on, job.title FROM appliers left join job on appliers.job_id = job.id where applier_id = ?", array($UserSession->userId), 2);				
	    	if(count($result) > 0){  
			$bids =  $result;
		}else {
			$bids =  NULL;
		}

		$this->view->data = array('bids' => $bids);
	    }
	    
	    // @ In post request method
	    if( $request->isPost() ) {
		
	    }
	    
	} catch( Exception $e ) {
	    print ($e); exit;
	}
	
    }

	//@ Deepak Apointments Action
	public function apointmentAction(){
		
	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800);
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{   
                $result = $db->fetchAll("SELECT * from quotes where vendor_id = ? and apoint_date IS NOT NULL", array($sess->userId), 2);
		if($result){
			$quotes = $result;
			$users = $db->fetchAll('SELECT * from user_operations where operation_on = ? and terms = ?', array($sess->userId, 'apoint'), 2);
			$arr = array();
			foreach($users as $key => $value){
				$arr[] =  $value['operation_by'];
			}	
			$user_id = implode(',', $arr);
			$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($user_id)", array(), 2);
			if(!empty($media)) $me = $media; 
			else $me = NULL;
		}else{
			$quotes = NULL;
			$me = NULL;
		}                  
                $this->view->data = array('quotes'=>$quotes, 'media'=>$me);     

            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
    }
	 // @Dinesh Quotes for vendor from users.
    public function quoteAction() {
        // action body
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        
        // get default session namespace
	Zend_Session::rememberMe(604800);
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{   
                $result = $db->fetchAll("SELECT * from quotes where vendor_id = ? and apoint_date IS NULL", array($sess->userId), 2);
		if($result){
			$quotes = $result;
			$users = $db->fetchAll('SELECT * from user_operations where operation_on = ? and terms = ?', array($sess->userId, 'quote'), 2);
			$arr = array();
			foreach($users as $key => $value){
				$arr[] =  $value['operation_by'];
			}	
			$user_id = implode(',', $arr);
			$media = $db->fetchAll("SELECT * FROM media where section='user-avatar' and owner in ($user_id)", array(), 2);
			if(!empty($media)) $me = $media; 
			else $me = NULL;
		}else{
			$quotes = NULL;
			$me = NULL;
		}                  
                $this->view->data = array('quotes'=>$quotes, 'media'=>$me);     

            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }
        
    }
    // @vikrant Delete vendor Reviews
    public function deleteReviewAction(){
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
     
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        if($request->isGet()) {   
                         
            try{  
                 if( $id = $request->get('id') ) {
                    $delete = $db->delete( 'feedback_reviews', 'id = '.$id );
                    if($delete) {
                        $urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'reviews');
                            $this->_helper->redirector->gotoRoute($urlOptions);
                    }else{
                        $this->view->data = array('error'=>'Unable to delete Review, kindly retry !');     
                    }
                }
            } catch (Zend_Db_Adapter_Exception $e) {
    
            } 
        }

    }
    // @Dinesh delete vendor applied bids.
    public function deleteBidAction() {

	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
 	 
        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        
        if($request->isGet()) {   
                         
            try{  
		 if( $id = $request->get('id') ) {
			$delete = $db->delete( 'appliers', 'id = '.$id );
			if($delete) {
				$urlOptions = array('module'=>'vendor', 'controller'=>'dashboard', 'action'=>'bids');
		        	$this->_helper->redirector->gotoRoute($urlOptions);
			}else{
				$this->view->data = array('error'=>'Unable to delete bid, kindly retry !');		
			}
		}
 	    } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            } 
        }

   } 
   //@vikrant to View Review data
   public function viewReviewAction(){
        $request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();

        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }
        if($request->isGet())
        {
            try{
            $sql = 'SELECT * from feedback_reviews where id=?';
            $result = $db->fetchAll($sql, $params['id'] , 2);
            $result = $result[0];
            $sql = 'SELECT * from user where id=?';
            $result_user = $db->fetchAll($sql, $result['user_id'] , 2);
            $result['by_email']=$result_user[0]['email'];
            $result['by_name'] = $result_user[0]['first_name']." ".$result_user[0]['last_name'];
            $this->view->data = array('review'=>$result);
            //echo "<pre>"; print_r($result); die;
            }
            catch(Exception $e){

            }

        }

   }

   public function viewBidAction(){
	$request = new Zend_Controller_Request_Http;
        $db=Zend_Registry::get("db");
        $sess =  new Zend_Session_Namespace('VendorSession');
 	$request = Zend_Controller_Front::getInstance()->getRequest();
        $params = $request->getParams();

        if( !isset( $sess->userId ) ){
            $this->_redirector->gotoSimple('index', 'index' , 'default', array() );
        }

         if($request->isGet()) {                
            try{ 
		if( $params['id'] ) {               
		        $sql = 'SELECT * from job where id=?';
			$result = $db->fetchAll($sql, $params['id'] , 2);
              		//echo "<pre>"; print_r($result);die;
		      if( $result ) {
			    $messages = $db->fetchAll("select m.id,m.conv_id, m.sent_to,m.by_from,m.message,m.date,u.first_name, u.last_name,u.email from messages as m left join conversation as c on m.conv_id = c.id left join user as u on m.by_from = u.id  where c.job_thread = ? and m.status in (0,1)", 'job_'.$params['id'] , 2);   
			    if(count($messages) > 0 ){
				$jobMess = $messages;
			    }else{
				$jobMess = NULL;
			    }                  
		            $this->view->data = array('method' => $this->getRequest()->getMethod(), 'job'=> $result, 'messages' => $jobMess);                    
		        } else {                    
		            $this->view->data = NULL;                    
		        }    
		}           
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }   // echo "<pre>";   print_r($result);die;  
        }

	if($request->isPost()) {
            try{
                if(!empty($_POST['conv_id'])) {
			$conv_id = $_POST['conv_id'];

		}else{
			  $conversation = array(
		            'name'      =>  $_POST['job_thread'],
		            'with_who'  =>  $_POST['with_who'],
		            'by_who'    =>  $sess->userId,
			    'job_thread'=>  $_POST['job_thread'],
		            'date'      =>  date('Y-m-d H:i:s')
		        );
		        $convers = $db->insert('conversation', $conversation);
			if( $convers ){
                    		$conv_id = $db->lastInsertId('conversation');
			}		
		}
                if( $conv_id ){
                   	
                    $data = array(
                        'conv_id' => $conv_id,
                        'sent_to' => $_POST['with_who'],
                        'by_from' => $sess->userId,
                        'message' => $_POST['message'],
                        'date'    => date('Y-m-d H:i:s'),
                        'status'  =>  0
                    );
                    $n = $db->insert( 'messages', $data );
                    if( $n ) {
			$this->_redirector->gotoSimple('view-bid', 'dashboard' , null , array('id' => $_POST['job_id']) );
                        /*$data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp'=> 'success');
                        $this->view->data  = $data;*/
                    } else {
                        $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                        $this->view->data  = $data;
                    }
                }else {
                    $data = array('method' => $this->getRequest()->getMethod(), 'data' =>false, 'resp' => 'error');
                    $this->view->data  = $data;
                }
            } catch (Zend_Db_Adapter_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
        
            } catch (Zend_Exception $e) {
                $data = array('method'=>$this->getRequest()->getMethod(), 'data'=>false, 'error'=>$e);
                $this->view->data  = $data;
            }
        }
   }
//********************************************************* Angular functions  Start
   public function accountinfoAction (){
        $UserSession = new Zend_Session_Namespace('VendorSession');
        $db=Zend_Registry::get("db"); 

        $email = $UserSession->userEmail;
        
        $sql2 = 'SELECT * FROM user WHERE email = ?';
        $user = $db->fetchAll($sql2, $email);

        $status = $user[0]->status;
        $user_id = $user[0]->id;
        
        $sql = 'SELECT * FROM vendor_category';                  //fetch all categories
        $category = $db->fetchAll($sql);
     
        $sql1 = 'SELECT * FROM countries';                        //fetch all countries
        $countries = $db->fetchAll($sql1);

        $sql3 = 'SELECT * FROM vendor WHERE email = ?';
        $vendor = $db->fetchAll($sql3, $email);

        $country = $vendor[0]->country;
        if($country){
            $sql3 = 'SELECT * FROM countries WHERE short_name = ?';   //fetch vendor's country_id
            $vendor1 = $db->fetchAll($sql3, $country);
            $country_id = $vendor1[0]->country_id;
         }
         else{
            $country_id = 0;
         }
        $media1 = $db->fetchAll("SELECT * FROM media WHERE owner =? and section =?", array($user_id,'user-avatar'), 2);
        $videos = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorvideo'), 2);
        $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($user_id, 'vendorgallery'), 2);

        
        $data = array('data'=> $category,'country'=>$countries,'status'=>$status,'user'=>$user,'vendor'=>$vendor,'country_id'=>$country_id,'media1'=>$media1, 'videos'=>$videos, 'gallery'=>$gallery);
          
        print_r(json_encode($data)); die;
        if( !isset($UserSession->userId ) ){
            $urlOptions = array('module'=>'users', 'controller'=>'index', 'action'=>'index');
            $this->_helper->redirector->gotoRoute($urlOptions);
        }
       
        

   }

   public function accountinfosaveAction(){
    $postdata = file_get_contents("php://input");

    $db=Zend_Registry::get("db"); 
    $data = (array)json_decode($postdata);
    
    if(isset($data['languages']))
        {
            $languages = implode(', ', $data['languages']);
            $data['languages'] = $languages;
        }
      
    $email = $data['email'];
    if(isset($data['fname'])) { $data['first_name'] = $data['fname']; unset($data['fname']);}
    if(isset($data['lname'])) { $data['last_name'] = $data['lname']; unset($data['lname']);}

    unset($data['email']);
    unset($data['lang']);    
    unset($data['selectedlang']);    
    //print_r($data); die;   
    $where = array("email = ?" => "$email");

    $query =  $db->update('vendor', $data, $where);
    
    print_r($query);
    die;
   }

//*********************************************************** Angular functions  end


}
