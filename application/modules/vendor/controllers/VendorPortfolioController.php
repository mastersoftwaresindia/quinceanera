<?php

class Vendor_VendorPortfolioController extends Zend_Controller_Action
{
    protected $_redirector = null;

    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('album-images', 'html')
                    ->initContext();
        
    }

    public function indexAction() {

        $request = new Zend_Controller_Request_Http;        
        if($request->isGet()) {    
            $db=Zend_Registry::get("db");
            $getParams = Zend_Controller_Front::getInstance()->getRequest();
            $params = $getParams->getParams();
            if( $params['id'] ){
                $videos = $db->fetchAll("select * from media where owner=? and section in ('vendorvideoimage', 'vendorvideo') and status=?", array($params['id'], '1'), 2);
                $gallery = $db->fetchAll('select * from media where owner=? and section=?', array($params['id'], 'vendorgallery'), 2);   
		$company =  $db->fetchAll('select * from vendor where user_id=?', array($params['id']), 2);

                $albums = $db->fetchAll('select * from albums where user_id=? order by id desc ', array($params['id']),2); 
                $this->view->data = array('videos'=>$videos,'gallery'=>$gallery,'albums'=>$albums,'images'=> $gallery , 'company' => $company);
                //echo "<pre>";print_r($albums);print_r($gallery);die;
            }else{
                $urlOptions = array('module'=>'vendor', 'controller'=>'index'); 
                $this->_helper->redirector->gotoRoute($urlOptions);
            }
        }
    }
    
    public function albumImagesAction(){
        $db=Zend_Registry::get("db");
        $albumsImages = $db->fetchAll('select * from media where album_id=? and section=?', array($_POST['id'], 'vendorgallery'), 2); 
        $this->view->data = array('method'=> $this->getRequest()->getMethod(),'images'=> $albumsImages);
    }
    
}

